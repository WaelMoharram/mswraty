<?php 

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{

  /**
   * Display a listing of the resource.
   *
   * @return Response
   */
  public function index()
  {
      $categories = Category::paginate(12);
      return view('dashboard.categories.index',compact('categories'));
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return Response
   */
  public function create()
  {
      $model = new Category;
      return view('dashboard.categories.create',compact('model'));
  }

  /**
   * Store a newly created resource in storage.
   *
   * @return Response
   */
  public function store(Request $request)
  {
      $rules = [
          'name_ar' => 'required',
          'name_en' => 'required',
          'icon' => 'required',
      ];

      $messages = [
          'name_ar.required' => 'الاسم مطلوب',
          'name_en.required' => 'الاسم مطلوب',
          'icon.required' => 'صورة القسم مطلوبة',
      ];

      $this->validate($request,$rules,$messages);
      $category = Category::create($request->except('icon'));
      if($request->hasFile('icon')){
          $path = base_path();

          $destinationPath = $path.'/uploads'; // upload path
          $photo= $request->file('icon');
          $extension = $photo->getClientOriginalExtension(); // getting image extension
          $name = time().''.rand(11111,99999).'.'.$extension; // renaming image
          $photo->move($destinationPath, $name); // uploading file to given path
          $category->icon  = 'uploads/'.$name ;
          $category->save();
      }

      if ($category) {
          flash()->success('تمت الاضافة');
          return redirect('admin/category');
      }else{
          flash()->error('حدث خطأ');
          return back();
      }
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function show($id)
  {
      $model = Category::find($id);
      return view('dashboard.categories.index',compact('model'));
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function edit($id)
  {
      $model = Category::find($id);
      return view('dashboard.categories.edit',compact('model'));
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function update(Request $request,$id)
  {
      $rules = [
          'name_ar' => 'required',
          'name_en' => 'required',
      ];

      $messages = [
          'name_ar.required' => 'الاسم مطلوب',
          'name_en.required' => 'الاسم مطلوب',
      ];

      $this->validate($request,$rules,$messages);

      $category = Category::find($id);
      $category2 = $category->update($request->except('icon'));
      if($request->hasFile('icon')){
          $path = base_path();
          $destinationPath = $path.'/uploads'; // upload path
          $photo= $request->file('icon');
          $extension = $photo->getClientOriginalExtension(); // getting image extension
          $name = time().''.rand(11111,99999).'.'.$extension; // renaming image
          $photo->move($destinationPath, $name); // uploading file to given path
          $category->icon  = 'uploads/'.$name ;
          $category->save();
      }

      if ($category2) {
          flash()->success('تمم التعديل');
          return redirect('admin/category');
      }else{
          flash()->error('حدث خطأ');
          return back();
      }
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function destroy($id)
  {
      $distroy = Category::findOrFail($id);

      $distroy->delete();

      if ($distroy) {
          flash()->success('تم حذف القسم بنجاح');
          return redirect('admin/category');
      }else{
          flash()->error('حدث خطأ');
          return back();
      }
  }

    public function enabled($id)
    {
        $model = Category::findOrFail($id);
        $model->enabled = 1;
        $model->save();
        flash()->success('تم التفعيل');
        return back();
    }

    public function disabled($id)
    {
        $model = Category::findOrFail($id);
        $model->enabled = 0;
        $model->save();
        flash()->success('تم إلغاء التفعيل');
        return back();
    }

  
}

?>