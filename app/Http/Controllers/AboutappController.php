<?php 

namespace App\Http\Controllers;

use App\Models\Aboutapp;
use Illuminate\Http\Request;

class AboutappController extends Controller
{

  /**
   * Display a listing of the resource.
   *
   * @return Response
   */
  public function index()
  {
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return Response
   */
  public function create()
  {
    
  }

  /**
   * Store a newly created resource in storage.
   *
   * @return Response
   */
  public function store()
  {
    
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function show($id)
  {
    
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function edit($id)
  {
      $model = Aboutapp::find($id);
      return view('dashboard.aboutapp.edit',compact('model'));
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function update($id,Request $request)
  {
      $about = Aboutapp::find($id);
      $about = $about->update($request->all());

      if ($about) {
          flash()->success('تم التعديل');
          return redirect('admin/about_app/'.$id.'/edit');
      }else{
          flash()->error('حدث خطأ');
          return back();
      }

  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function destroy($id)
  {
    
  }
  
}

?>