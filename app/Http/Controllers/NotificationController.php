<?php 

namespace App\Http\Controllers;

use App\Models\Token;
use Illuminate\Http\Request;

class NotificationController extends Controller
{

    public function send(Request $request)
    {
        //get user token
        //$tokens='';
        if ($request->to==0){
            $to = ['included_segments' => array('All')];
            //$tokens=Token::pluck('token')->toArray();
        }elseif ($request->to==1){
            $to = ['included_segments' => array('Photographers')];
            //$tokens=Token::where('tokenable_type','App\Models\Photographer')->pluck('token')->toArray();
        }elseif ($request->to==2){
            $to = ['included_segments' => array('Customers')];
            //$tokens=Token::pluck('token')->where('tokenable_type','App\Models\Customer')->toArray();
        }

        //dd($tokens);

        //$to = ['include_player_ids' => $tokens];
        $msg_ar=$request->msg_ar;
        $msg_en=$request->msg_en;
        $contents = [
            'en' => $msg_en,
            'ar' => $msg_ar,
        ];

        //dd($notification);
        $send = oneSignalNotification($to , $contents , [
            'user_type' => 'admin',
            'action' => 'send notification'
        ]);

        if ($send) {
            flash()->success($send);
            return redirect('admin/notification');
        }else{
            flash()->error('حدث خطأ');
            return back();
        }
    }

    public function view()
    {
        return view('dashboard.notifications.create');
    }

}

?>