<?php 

namespace App\Http\Controllers;

use App\Models\Photographer;
use Illuminate\Http\Request;

class PhotographerController extends Controller
{

  /**
   * Display a listing of the resource.
   *
   * @return Response
   */
  public function index(Request $request)
  {
      $photographers = Photographer::where(function($query) use($request){

          if($request->has('name'))
          {
              $query->where('name','LIKE','%'.$request->name.'%');
          }

          if($request->has('mobile'))
          {
              if($request->mobile!=''||$request->mobile!=NULL){
                  $query->where('mobile','LIKE','%'.$request->mobile.'%');
              }

          }

          if($request->has('gender'))
          {
              if($request->gender!='0'){
                  $query->where('gender',$request->gender);
              }

          }


      })->orderBy('created_at', 'desc')->paginate(12)->appends($request->all());
      return view('dashboard.photographers.index',compact('photographers'));
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return Response
   */
  public function create()
  {
    
  }

  /**
   * Store a newly created resource in storage.
   *
   * @return Response
   */
  public function store()
  {
    
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function show($id)
  {
    
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function edit($id)
  {
    
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function update($id)
  {
    
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function destroy($id)
  {
    
  }


    public function enabled($id)
    {
        $model = Photographer::findOrFail($id);
        $model->enabled = 1;
        $model->save();
        flash()->success('تم التفعيل');
        return back();
    }

    public function disabled($id)
    {
        $model = Photographer::findOrFail($id);
        $model->enabled = 0;
        $model->save();
        flash()->success('تم إلغاء التفعيل');
        return back();
    }

}

?>