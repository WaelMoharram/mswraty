<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Transaction;
use App\Models\Photographer;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $transactions = Transaction::get();
        return view('dashboard.home',compact('transactions'));
    }

    public function report()
    {
        $photographers = Photographer::orderBy('created_at', 'desc')->paginate(12);
        return view('dashboard.profit_report.index',compact('photographers'));
    }

    public function photographer_transactions($id)
    {
        $photographer=Photographer::find($id);
        $transactions = $photographer->transactions()->paginate(20);


        return view('dashboard.transaction_report.index',compact('transactions','photographer'));
    }

}
