<?php 

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{

  /**
   * Display a listing of the resource.
   *
   * @return Response
   */
  public function index()
  {
      $admins = User::paginate(12);
      return view('dashboard.admins.index',compact('admins'));
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return Response
   */
  public function create()
  {
      $model = new User;
      return view('dashboard.admins.create',compact('model'));
  }

  /**
   * Store a newly created resource in storage.
   *
   * @return Response
   */
  public function store(Request $request)
  {
      $rules = [
          'name' => 'required',
          'email' => 'required|email|unique:admins,email',
          'password' => 'required|confirmed|min:6',
      ];

      $messages = [
          'name.required' => 'الاسم مطلوب',
          'email.required' => 'البريد الالكترونى مطلوب',
          'email.email' => 'يجب كتابة بريد الكترونى صحيح',
          'email.unique' => 'البريد الاكترونى مستخدم من قبل',
          'password.required' => 'كلمة المرور مطلوبة',
          'password.confirmed' => 'كلمة المرور غير مطابقة',
          'password.min:6' =>'كلمة المرور يجب ان تكون 6 احرف على الأقل',

      ];

      $this->validate($request,$rules,$messages);

      $password = bcrypt($request->password);
      $request->merge(['password' => $password]);
      $admin = User::create($request->all());


      if ($admin) {
          flash()->success('تمت الاضافة');
          return redirect('admin/admin');
      }else{
          flash()->error('حدث خطأ');
          return back();
      }
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function show($id)
  {
    
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function edit($id)
  {
      $model = User::find($id);
      return view('dashboard.admins.edit',compact('model'));
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function update(Request $request,$id)
  {
      //dd($request->all());
      $rules = [
          'name' => 'required',
          'email' => 'required|email|unique:admins,email,'.$id,
      ];
      if (!is_null($request->password))
      {
          $rules['password'] = 'confirmed|min:6';
      }



      $messages = [
          'name.required' => 'الاسم مطلوب',
          'email.required' => 'البريد الالكترونى مطلوب',
          'email.email' => 'يجب كتابة بريد الكترونى صحيح',
          'email.unique' => 'البريد الاكترونى مستخدم من قبل',
          'password.confirmed' => 'كلمة المرور غير مطابقة',
          'password.min:6' =>'كلمة المرور يجب ان تكون 6 احرف على الأقل',

      ];

      $this->validate($request,$rules,$messages);

      $admin = User::findOrFail($id);
      $admin->update($request->except('password'));
      if (!is_null($request->password))
      {
          $admin->password =  bcrypt($request->password);
          $admin->save();
      }


      if ($admin) {
          flash()->success('تم التعديل');//upload
          return redirect('admin/admin');
      }else{
          flash()->error('حدث خطأ');
          return back();
      }
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function destroy($id)
  {
      $user = User::findOrFail($id);

      $user->delete();

      if ($user) {
          flash()->success('تم مسح المدير بنجاح');
          return redirect('admin/admin');
      }else{
          flash()->error('حدث خطأ');
          return back();
      }
  }

}

?>