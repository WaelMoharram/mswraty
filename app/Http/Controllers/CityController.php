<?php 

namespace App\Http\Controllers;

use App\Models\City;
use Illuminate\Http\Request;

class CityController extends Controller
{

  /**
   * Display a listing of the resource.
   *
   * @return Response
   */
  public function index()
  {
      $cities = City::paginate(12);
      return view('dashboard.cities.index',compact('cities'));
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return Response
   */
  public function create()
  {
      $model = new City;
      return view('dashboard.cities.create',compact('model'));
  }

  /**
   * Store a newly created resource in storage.
   *
   * @return Response
   */
  public function store(Request $request)
  {
      $rules = [
          'name_ar' => 'required',
          'name_en' => 'required',

      ];

      $messages = [
          'name_ar.required' => 'الاسم بالعربية مطلوب',
          'name_en.required' => 'الاسم بالانجليزية مطلوب',

      ];

      $this->validate($request,$rules,$messages);

      $request->country_id='1';

      $city = City::create($request->all());


      if ($city) {
          flash()->success('تمت الاضافة');
          return redirect('admin/city');
      }else{
          flash()->error('حدث خطأ');
          return back();
      }
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function show($id)
  {
      $model = City::find($id);
      return view('dashboard.cities.index',compact('model'));
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function edit($id)
  {
      $model = City::find($id);
      return view('dashboard.cities.edit',compact('model'));

  }

  /**
   * Update the specified resource in storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function update(Request $request,$id)
  {
      $rules = [
          'name_ar' => 'required',
          'name_en' => 'required',

      ];

      $messages = [
          'name_ar.required' => 'الاسم بالعربية مطلوب',
          'name_en.required' => 'الاسم بالانجليزية مطلوب',

      ];
      $request->merge(array('country_id' => '1'));

      $this->validate($request,$rules,$messages);



      $city = City::findOrFail($id);
      $city->update($request->all());



      if ($city) {
          flash()->success('تم التعديل');
          return redirect('admin/city');
      }else{
          flash()->error('حدث خطأ');
          return back();
      }
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function destroy($id)
  {
      $distroy = City::findOrFail($id);

      $distroy->delete();

      if ($distroy) {
          flash()->success('تم حذف المدينة بنجاح');
          return redirect('admin/city');
      }else{
          flash()->error('حدث خطأ');
          return back();
      }
  }
  
}

?>