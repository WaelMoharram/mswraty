<?php 

namespace App\Http\Controllers;

use App\Models\Reasonsmsg;
use Illuminate\Http\Request;

class ReasonsmsgController extends Controller
{

  /**
   * Display a listing of the resource.
   *
   * @return Response
   */
  public function index()
  {
      $reasons = Reasonsmsg::orderBy('created_at', 'desc')->paginate(12);
      return view('dashboard.reasons.index',compact('reasons'));
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return Response
   */
  public function create()
  {
      $model = new Reasonsmsg;
      return view('dashboard.reasons.create',compact('model'));
  }

  /**
   * Store a newly created resource in storage.
   *
   * @return Response
   */
  public function store(Request $request)
  {
      $rules = [
          'name_ar' => 'required',
          'name_en' => 'required',

      ];

      $messages = [
          'name_ar.required' => 'الاسم مطلوب',
          'name_en.required' => 'الاسم مطلوب',


      ];

      $this->validate($request,$rules,$messages);

      $admin = Reasonsmsg::create($request->all());


      if ($admin) {
          flash()->success('تمت الاضافة');
          return redirect('admin/reason');
      }else{
          flash()->error('حدث خطأ');
          return back();
      }
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function show($id)
  {
    
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function edit($id)
  {
      $model = Reasonsmsg::find($id);
      return view('dashboard.reasons.edit',compact('model'));
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function update(Request $request,$id)
  {
      $rules = [
          'name_ar' => 'required',
          'name_en' => 'required',

      ];

      $messages = [
          'name_ar.required' => 'الاسم مطلوب',
          'name_en.required' => 'الاسم مطلوب',


      ];

      $this->validate($request,$rules,$messages);

      $reason = Reasonsmsg::findOrFail($id);
      $reason->update($request->all());



      if ($reason) {
          flash()->success('تم التعديل');
          return redirect('admin/reason');
      }else{
          flash()->error('حدث خطأ');
          return back();
      }
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function destroy($id)
  {
      $reason = Reasonsmsg::findOrFail($id);

      $reason->delete();

      if ($reason) {
          flash()->success('تم مسح السبب بنجاح');
          return redirect('admin/reason');
      }else{
          flash()->error('حدث خطأ');
          return back();
      }
  }
  
}

?>