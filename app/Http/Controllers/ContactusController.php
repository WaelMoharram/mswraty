<?php 

namespace App\Http\Controllers;

use App\Models\Contactus;

class ContactusController extends Controller
{

  /**
   * Display a listing of the resource.
   *
   * @return Response
   */
  public function index()
  {

      $messages = Contactus::orderBy('created_at', 'desc')->paginate(12);
      return view('dashboard.messages.index',compact('messages'));
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return Response
   */
  public function create()
  {
    
  }

  /**
   * Store a newly created resource in storage.
   *
   * @return Response
   */
  public function store()
  {
    
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function show($id)
  {
    
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function edit($id)
  {
    
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function update($id)
  {
    
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function destroy($id)
  {
      $destroy = Contactus::findOrFail($id);

      $destroy->delete();

      if ($destroy) {
          flash()->success('تم مسح الرسالة بنجاح');
          return redirect('admin/message');
      }else{
          flash()->error('حدث خطأ');
          return back();
      }
  }
  
}

?>