<?php 

namespace App\Http\Controllers;

use App\Models\Customer;
use Illuminate\Http\Request;

class CustomerController extends Controller
{

  /**
   * Display a listing of the resource.
   *
   * @return Response
   */
  public function index(Request $request)
  {
      $customers = Customer::where(function($query) use($request){

          if($request->has('name'))
          {
              $query->where('name','LIKE','%'.$request->name.'%');
          }

          if($request->has('gender'))
          {
              if($request->gender!='0'){
                  $query->where('gender',$request->gender);
              }

          }

          if($request->has('city_id'))
          {
              if($request->city_id!='0'){
                  $query->where('city_id',$request->city_id);
              }

          }

      })->orderBy('created_at', 'desc')->paginate(12)->appends($request->all());
      return view('dashboard.customers.index',compact('customers'));
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return Response
   */
  public function create()
  {
    
  }

  /**
   * Store a newly created resource in storage.
   *
   * @return Response
   */
  public function store()
  {
    
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function show($id)
  {
    
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function edit($id)
  {
    
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function update($id)
  {
    
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function destroy($id)
  {
    
  }
    public function enabled($id)
    {
        $model = Customer::findOrFail($id);
        $model->enabled = 1;
        $model->save();
        flash()->success('تم التفعيل');
        return back();
    }

    public function disabled($id)
    {
        $model = Customer::findOrFail($id);
        $model->enabled = 0;
        $model->save();
        flash()->success('تم إلغاء التفعيل');
        return back();
    }
  
}

?>