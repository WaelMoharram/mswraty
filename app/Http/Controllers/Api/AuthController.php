<?php

namespace App\Http\Controllers\Api;

use App\Models\Customer;
use App\Models\Photographer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

class AuthController extends Controller
{

    //--------------------------------------------------------------Customers section
    //---------------------------------------------customer register
    public function CustomerRegister(Request $request)
    {
        $rules = [
            'name' => 'required',
            'mobile' => 'required',
            'email' => 'required|email|unique:customers,email',
            'password' => 'required'
        ];

        $messages= [
            'name.required' => trans('api.name.required'),
            'mobile.required' => trans('api.mobile.required'),
            'email.required' => trans('api.email.required'),
            'email.unique' => trans('api.email.unique'),
            'email.email' => trans('api.email.invalid'),
            'password.required' => trans('api.password.required'),
        ];

        $validation = validator()->make($request->all(), $rules,$messages);

        if ($validation->fails()) {

            return responseJson(0,$validation->errors()->first()) ;

        }

        $userToken = str_random(60);
        $request->merge(array('api_token' => $userToken));
        $request->merge(array('password' => bcrypt($request->password)));
        $user = Customer::create($request->all());

        if ($user) {
            $user = Customer::find($user->id);
            $user=$user->toArray();
            $user['api_token']=$userToken;

            return responseJson(1,trans('api.signup.success'),$user) ;
        } else {
            return responseJson(0,trans('api.signup.error')) ;

        }
    }

    //---------------------------------------------Customer login
    public function CustomerLogin(Request $request)
    {
        $rules = [
            'email' => 'required|email',
            'password' => 'required'
        ];

        $messages= [
            'email.required' => trans('api.email.required'),
            'email.email' => trans('api.email.invalid'),
            'password.required' => trans('api.password.required'),
        ];

        $validation = validator()->make($request->all(), $rules,$messages);

        if ($validation->fails()) {
            return responseJson(0,$validation->errors()->first()) ;
        }

        $user = Customer::where('email', $request->input('email'))->first();
        if ($user)
        {
            if ($user->enabled == 0)
            {

                return responseJson(0,trans('api.login.banned')) ;

            }
            if (Hash::check($request->password, $user->password))
            {
                $api_token=$user->api_token;
                $user=$user->toArray();
                $user['api_token']=$api_token;

                return responseJson(1,trans('api.login.success'),$user) ;

            }else{

                return responseJson(0,trans('api.login.notCorrect')) ;

            }
        }else{

            return responseJson(0,trans('api.login.notCorrect')) ;

        }
    }

    //---------------------------------------------Customer facebook login-sign up
    public function CustomerFacebook(Request $request)
    {
        $rules = [
            'email' => 'required',
            'fb_token' => 'required'
        ];

        $messages= [
            'email.required' => trans('api.email.required'),
            'fb_token.required' => trans('api.fb_token.required'),
        ];

        $validation = validator()->make($request->all(), $rules,$messages);

        if ($validation->fails()) {
            return responseJson(0,$validation->errors()->first()) ;
        }

        $user = Customer::where('email', $request->input('email'))->first();
        if ($user)
        {
            if ($user->enabled == 0)
            {
                return responseJson(0,trans('api.login.banned')) ;

            }else{
                $user->fb_token=$request->fb_token;
                $user->save();
                $api_token=$user->api_token;
                $user=$user->toArray();
                $user['api_token']=$api_token;

                return responseJson(1,trans('api.login.success'),$user) ;

            }

        }else{

            $userToken = str_random(60);
            $request->merge(array('api_token' => $userToken));
            $request->merge(array('name' => $request->email));
            $request->merge(array('fb_token' => $request->fb_token));
            $userCreate = Customer::create($request->all());

            if ($userCreate) {
                $userCreate = Customer::find($userCreate->id);
                $userCreate=$userCreate->toArray();
                $userCreate['api_token']=$userToken;
                return responseJson(1,trans('api.signup.success'),$userCreate) ;

            } else {
                return responseJson(0,trans('api.signup.error')) ;

            }
        }
    }

    //---------------------------------------------Customer google login-sign up
    public function CustomerGoogle(Request $request)
    {
        $rules = [
            'email' => 'required',
            'g_token' => 'required'
        ];

        $messages= [
            'email.required' => trans('api.email.required'),
            'g_token.required' => trans('api.g_token.required'),
        ];

        $validation = validator()->make($request->all(), $rules,$messages);

        if ($validation->fails()) {
            return responseJson(0,$validation->errors()->first()) ;

        }

        $user = Customer::where('email', $request->input('email'))->first();
        if ($user)
        {
            if ($user->enabled == 0)
            {
                return responseJson(0,trans('api.login.banned')) ;

            }else{
                $user->g_token=$request->g_token;
                $api_token=$user->api_token;
                $user=$user->toArray();
                $user['api_token']=$api_token;
                return responseJson(1,trans('api.login.success'),$user) ;

            }

        }else{

            $userToken = str_random(60);
            $request->merge(array('api_token' => $userToken));
            $request->merge(array('name' => $request->email));
            $request->merge(array('g_token' => $request->g_token));
            $userCreate = Customer::create($request->all());
            if ($userCreate) {

                $userCreate = Customer::find($userCreate->id);
                $userCreate=$userCreate->toArray();
                $userCreate['api_token']=$userToken;

                return responseJson(1,trans('api.signup.success'),$userCreate) ;

            } else {
                return responseJson(0,trans('api.signup.error')) ;

            }
        }
    }

    //---------------------------------------------Customer reset password
    public function customerReset(Request $request)
    {

        $rules = [
            'email' => 'required'
        ];

        $messages= [
            'email.required' => trans('api.email.required'),
        ];

        $validation = validator()->make($request->all(), $rules,$messages);

        if ($validation->fails()) {
            return responseJson(0,$validation->errors()->first()) ;
        }

        $user = Customer::where('email',$request->email)->first();
        if ($user){
            $code = rand(111111,999999);
            $update = $user->update(['reset_code' => $code]);
            if ($update)
            {
                // send email
//                Mail::send('emails.reset', ['reset_code' => $code], function ($mail) use($user) {
//                    $mail->from('mswrati.application@gmail.com', 'تطبيق مصوراتى');
//
//                    $mail->to($user->email, $user->name)->subject('إعادة تعيين كلمة المرور');
//                });

                return responseJson(1,trans('api.email.check'));
            }else{
                return responseJson(0,trans('api.email.error'));
            }
        }else{
            return responseJson(0,trans('api.email.no'));
        }
    }

    //--------------------------------------------------------------Photographers section
    //---------------------------------------------photographer register
    public function PhotographerRegister(Request $request)
    {
        $rules = [
            'name' => 'required',
            'mobile' => 'required',
            'email' => 'required|email|unique:photographers,email',
            'password' => 'required'
        ];

        $messages= [
            'name.required' => trans('api.name.required'),
            'mobile.required' => trans('api.mobile.required'),
            'email.required' => trans('api.email.required'),
            'email.unique' => trans('api.email.unique'),
            'email.email' => trans('api.email.invalid'),
            'password.required' => trans('api.password.required'),
        ];

        $validation = validator()->make($request->all(), $rules,$messages);

        if ($validation->fails()) {

            return responseJson(0,$validation->errors()->first()) ;

        }

        $userToken = str_random(60);
        $request->merge(array('api_token' => $userToken));
        $request->merge(array('password' => bcrypt($request->password)));
        $user = Photographer::create($request->all());

        if ($user) {
            $user = Photographer::find($user->id);
            $user=$user->toArray();
            $user['api_token']=$userToken;

            return responseJson(1,trans('api.signup.success'),$user) ;
        } else {
            return responseJson(0,trans('api.signup.error')) ;

        }
    }

    //---------------------------------------------Photographer login
    public function PhotographerLogin(Request $request)
    {
        $rules = [
            'email' => 'required|email',
            'password' => 'required'
        ];

        $messages= [
            'email.required' => trans('api.email.required'),
            'email.email' => trans('api.email.invalid'),
            'password.required' => trans('api.password.required'),
        ];

        $validation = validator()->make($request->all(), $rules,$messages);

        if ($validation->fails()) {
            return responseJson(0,$validation->errors()->first()) ;
        }

        $user = Photographer::where('email', $request->input('email'))->first();
        if ($user)
        {
            if ($user->enabled == 0)
            {

                return responseJson(0,trans('api.login.banned')) ;

            }
            if (Hash::check($request->password, $user->password))
            {
                $api_token=$user->api_token;
                $user=$user->toArray();
                $user['api_token']=$api_token;

                return responseJson(1,trans('api.login.success'),$user) ;

            }else{

                return responseJson(0,trans('api.login.notCorrect')) ;

            }
        }else{

            return responseJson(0,trans('api.login.notCorrect')) ;

        }
    }

    //---------------------------------------------Photographer facebook login-sign up
    public function PhotographerFacebook(Request $request)
    {
        $rules = [
            'email' => 'required',
            'fb_token' => 'required'
        ];

        $messages= [
            'email.required' => trans('api.email.required'),
            'fb_token.required' => trans('api.fb_token.required'),
        ];

        $validation = validator()->make($request->all(), $rules,$messages);

        if ($validation->fails()) {
            return responseJson(0,$validation->errors()->first()) ;
        }

        $user = Photographer::where('email', $request->input('email'))->first();
        if ($user)
        {
            if ($user->enabled == 0)
            {
                return responseJson(0,trans('api.login.banned')) ;

            }else{
                $user->fb_token=$request->fb_token;
                $user->save();
                $api_token=$user->api_token;
                $user=$user->toArray();
                $user['api_token']=$api_token;

                return responseJson(1,trans('api.login.success'),$user) ;

            }

        }else{

            $userToken = str_random(60);
            $request->merge(array('api_token' => $userToken));
            $request->merge(array('name' => $request->email));
            $request->merge(array('fb_token' => $request->fb_token));
            $userCreate = Photographer::create($request->all());

            if ($userCreate) {
                $userCreate = Photographer::find($userCreate->id);
                $userCreate=$userCreate->toArray();
                $userCreate['api_token']=$userToken;
                return responseJson(1,trans('api.signup.success'),$userCreate) ;

            } else {
                return responseJson(0,trans('api.signup.error')) ;

            }
        }
    }

    //---------------------------------------------Photographer google login-sign up
    public function PhotographerGoogle(Request $request)
    {
        $rules = [
            'email' => 'required',
            'g_token' => 'required'
        ];

        $messages= [
            'email.required' => trans('api.email.required'),
            'g_token.required' => trans('api.g_token.required'),
        ];

        $validation = validator()->make($request->all(), $rules,$messages);

        if ($validation->fails()) {
            return responseJson(0,$validation->errors()->first()) ;

        }

        $user = Photographer::where('email', $request->input('email'))->first();
        if ($user)
        {
            if ($user->enabled == 0)
            {
                return responseJson(0,trans('api.login.banned')) ;

            }else{
                $user->g_token=$request->g_token;
                $api_token=$user->api_token;
                $user=$user->toArray();
                $user['api_token']=$api_token;
                return responseJson(1,trans('api.login.success'),$user) ;

            }

        }else{

            $userToken = str_random(60);
            $request->merge(array('api_token' => $userToken));
            $request->merge(array('name' => $request->email));
            $request->merge(array('g_token' => $request->g_token));
            $userCreate = Photographer::create($request->all());

            if ($userCreate) {

                $userCreate = Photographer::find($userCreate->id);
                $userCreate=$userCreate->toArray();
                $userCreate['api_token']=$userToken;

                return responseJson(1,trans('api.signup.success'),$userCreate) ;

            } else {
                return responseJson(0,trans('api.signup.error')) ;

            }
        }
    }

    //---------------------------------------------Photographer reset password
    public function photographerReset(Request $request)
    {

        $rules = [
            'email' => 'required'
        ];

        $messages= [
            'email.required' => trans('api.email.required'),
        ];

        $validation = validator()->make($request->all(), $rules,$messages);

        if ($validation->fails()) {
            return responseJson(0,$validation->errors()->first()) ;
        }

        $user = Photographer::where('email',$request->email)->first();
        if ($user){
            $code = rand(111111,999999);
            $update = $user->update(['reset_code' => $code]);
            if ($update)
            {
                // send email
//                Mail::send('emails.reset', ['reset_code' => $code], function ($mail) use($user) {
//                    $mail->from('mswrati.application@gmail.com', 'تطبيق مصوراتى');
//
//                    $mail->to($user->email, $user->name)->subject('إعادة تعيين كلمة المرور');
//                });

                return responseJson(1,trans('api.email.check'));
            }else{
                return responseJson(0,trans('api.email.error'));
            }
        }else{
            return responseJson(0,trans('api.email.no'));
        }
    }

}
