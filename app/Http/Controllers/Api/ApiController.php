<?php

namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use App\Models\Aboutapp;
use App\Models\Category;
use App\Models\City;
use App\Models\Contactus;
use App\Models\Customer;
use App\Models\Notification;
use App\Models\Photographer;
use App\Models\Portfolio;
use App\Models\Reasonsmsg;
use App\Models\Reservation;
use App\Models\Service;
use App\Models\Token;
use App\Models\Transaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class ApiController extends Controller
{

    // categories
    public function Categories()
    {
        $categories = Category::enabled()->get();

        return responseJson(1,trans('api.back.success'),$categories) ;

    }

    // cities
    public function Cities()
    {
        $cities = City::enabled()->get();

        return responseJson(1,trans('api.back.success'),$cities) ;

    }

    // reservations
    public function Reservations(Request $request)
    {
        $relations = [];
        if(Auth::guard('photographer')->check()){
            $relations[] = 'customer';
//                $q->with('customer');
        }elseif(Auth::guard('customer')->check()){
            $relations[] = 'photographer';
//                $q->with('photographer');
        }
        $reservations = $request->user()->reservations()->where('status',$request->status)->where('rejected', 0)->with($relations)->with('service')->latest()->paginate(30);

        return responseJson(1,trans('api.back.success'),$reservations) ;
    }

    // About app
    public function AboutApp()
    {
        $about = Aboutapp::first();
        $reasons = Reasonsmsg::all();
        $about->reasons=$reasons;


        return responseJson(1,trans('api.back.success'),$about) ;


    }

    // contact us reasons
    public function Reasons()
    {
        $reasons = Reasonsmsg::all();
        return responseJson(1,trans('api.back.success'),$reasons) ;


    }

    // Send New Message From Client (contact us)
    public function ContactUs(Request $request)
    {

        $rules = [
            'email' => 'required|email',
            'reason_for_contact_id' => 'required',
            'message' => 'required',
        ];

        $messages= [

            'email.required' => trans('api.email.required'),
            'email.email' => trans('api.email.invalid'),
            'mobile.required' => trans('api.mobile.required'),
            'password.required' => trans('api.password.required'),
        ];

        $validation = validator()->make($request->all(), $rules,$messages);

        if ($validation->fails()) {
            return responseJson(0,$validation->errors()->first()) ;

        }

        $ContactUs = Contactus::create($request->all());
        if ($ContactUs) {

            return responseJson(1,trans('api.save_message.success')) ;


        } else {
            return responseJson(0,trans('api.save_message.error')) ;

        }
    }

    // Rate reservation
    public function Rate(Request $request)
    {

        $rules = [
            'reservation_id' => 'required',
            'rate' => 'required',
        ];

        $messages= [
            'reservation_id.required' => trans('api.reservation_id.required'),
            'rate.required' => trans('api.rate.required'),
        ];

        $validation = validator()->make($request->all(), $rules,$messages);

        if ($validation->fails()) {
            return responseJson(0,$validation->errors()->first()) ;

        }

        $reservation=Reservation::where('status','done')->where('rejected',FALSE)->find($request->reservation_id);
        if ($reservation) {

            $Rate = $reservation->update(array('rate'=>$request->rate));
            if ($Rate) {

                //send notification to photographer
                //get user token
                $tokens=$reservation->photographer->tokens()->pluck('token')->toArray();
                $customerName=$reservation->customer->name;
                $serviceName=$reservation->service->name;
                //dd($tokens);
                $to = ['include_player_ids' => $tokens];
                $msg_ar='قام '.$customerName.' بتقييم خدمة '.$serviceName.' ';
                $msg_en=$customerName.' rate your service '.$serviceName;
                $notification = [
                    'en' => $msg_en,
                    'ar' => $msg_ar,
                ];
                //dd($notification);
                oneSignalNotification($to , $notification , [
                    'user_type' => 'photographer',
                    'action' => 'rating',
                    'reservation_id' => $request->reservation_id,
                ]);
                //save notification
                $noti= [];
                $noti['photographer_id'] = $reservation->photographer_id;
                $noti['msg_ar'] = $msg_ar;
                $noti['msg_en'] = $msg_en;
                $noti['object_id'] = $request->reservation_id;
                $noti['object_type'] = 'rating';
                Notification::create($noti);
                return responseJson(1,trans('api.rate.success')) ;
            } else {
                return responseJson(0,trans('api.rate.error')) ;

            }
        } else {
            return responseJson(0,trans('api.rate.error')) ;
        }
    }

    // Photographers
    public function photographers(Request $request)
    {
        $photographers = Photographer::where(function($q) use($request) {

            if ($request->has('category_id')  && $request->category_id != NULL) {
                $q->category($request->category_id);
            }

            if ($request->has('city_id') && $request->city_id != NULL)
            {
                $q->where('city_id', $request->city_id);
            }

            if ($request->has('gender') && $request->gender != NULL)
            {
                $q->where('gender', $request->gender);
            }


//            if ($request->has('rate'))
//            {
//
//                    $q->where(reservation::avg('rate'),$request->rate);
//
//            }
//            if ($request->has('rate'))
//            {
//
//
//                    $q->whereHas('reservations',function($q1) use($request) {
//                        $q1->whereRaw('avg(reservations.rate) = '.$request->rate);
//                        $q1->groupBy('photographer_id');
//                    });
//
//
//            }

            if ($request->has('keyword') && $request->keyword != NULL)
            {
                $keyword = $request->keyword;

                $q->where(function ($q2) use($keyword) {

                    $q2->whereHas('services',function($q1) use($keyword) {
                        $q1->where('name','LIKE', '%'.$keyword.'%');
                    });

                    $q2->orWhere('name', 'LIKE', '%'.$keyword.'%');
                    $q2->orWhere('description', 'LIKE', '%'.$keyword.'%');

                });


            }

            if ($request->has('price_from') && $request->price_from != NULL)
            {
                $q->whereHas('services',function($q1) use($request) {
                    $q1->where('price','>=',$request->price_from);
                });
            }

            if ($request->has('price_to') && $request->price_to != NULL)
            {
                $q->whereHas('services',function($q1) use($request) {
                    $q1->where('price','<=',$request->price_to);
                });
            }

        })->enabled()->paginate(30);

        return responseJson(1,trans('api.back.success'),$photographers) ;
    }

    // Photographer
    public function photographer(Request $request)
    {
        $photographer = Photographer::enabled()->find($request->id);

//        $photographer = Photographer::enabled()->with('services')->find($request->id);
//        $photographer->append('portfolios_by_categories');

        return responseJson(1,trans('api.back.success'),$photographer) ;
    }

    // Photographer portfolio
    public function photographerPortfolio(Request $request)
    {

        $photographer = Photographer::find($request->id);
        $categories = $photographer->portfolios_by_categories;

        /*$portfolio =Category::with('portfolios')->whereHas('portfolios',function ($q) use ($request){
            $q->where('photographer_id',$request->id);
        })->get();*/
        return responseJson(1,trans('api.back.success'),$categories) ;
    }

    //Show & Edit Customer profile
    public function customerProfile(Request $request)
    {

        $user = $request->user();

        if ($request->has('name') && $request->name != NULL) {
            $user->update($request->only('name'));
        }

        if ($request->has('gender') && $request->gender != NULL) {
            $user->update($request->only('gender'));
        }
        if ($request->has('mobile') && $request->mobile != NULL) {
            $user->update($request->only('mobile'));
        }
        if ($request->has('city_id') && $request->city_id != NULL) {
            $user->update($request->only('city_id'));
        }
        if ($request->has('notification_state')) {
            $state = 1;
            if ($request->notification_state == 'false' || $request->notification_state == '0') {
                $state = 0;
            }
            $user->update(['notification_state' => $state]);
        }

        if ($request->hasFile('image') && $request->image != NULL)
        {
            $image = $request->file('image');
            $destinationPath = base_path() . '/uploads';
            $extension = $image->getClientOriginalExtension(); // getting image extension
            $name = time() . '' . rand(11111, 99999) . '.' . $extension; // renameing image
            $image->move($destinationPath, $name); // uploading file to given
            $user->update(['image' => 'uploads/' . $name]);
            //fitImage('uploads/' . $name,200);
        }

        $data = [
            'user' => Customer::find($user->id)
        ];

        return responseJson(1,trans('api.update.success'),$data);

    }

    //Show & Edit Customer profile
    public function customer_change_notification_state(Request $request)
    {

        $rules = [
            'notification_state' => 'required',
        ];

        $messages= [
            'notification_state.required' => trans('api.notification_state.required'),
        ];

        $validation = validator()->make($request->all(), $rules, $messages);

        if ($validation->fails()) {
            return responseJson(0,$validation->errors()->first()) ;
        }

        $user = $request->user();

        if ($request->has('notification_state')) {
            $state = 1;
            if ($request->notification_state == 'false' || $request->notification_state == '0') {
                $state = 0;
            }
            $user->update(['notification_state' => $state]);
        }

        $data = [
            'user' => Customer::find($user->id)
        ];

        return responseJson(1,trans('api.update.success'),$data);

    }

    //change password
    public function changePassword(Request $request){
        // 1. check if customer
        $user='';
        $user = Auth::guard('customer')->user();

        if (!$user) {
            // 2. check if photographer
            $user = Auth::guard('photographer')->user();
        }

        if (!$user) {
            return responseJson(401, 'not authenticated');
        }

        $rules = [
            'password' => 'required|confirmed|min:6',
            'old_password' => 'required'
        ];

        $messages= [
            'password.required' => trans('api.password.required'),
            'password.confirmed' => trans('api.password.confirmed'),
            'password.min:6' => trans('api.password.min'),
            'old_password.required' => trans('api.old_password.required'),
        ];

        $validation = validator()->make($request->all(), $rules,$messages);

        if ($validation->fails()) {
            return responseJson(0,$validation->errors()->first()) ;

        }
        if (Hash::check($request->old_password,$user->password)){
            $user->password=bcrypt($request->password);
            $user->save();
            return responseJson(1,trans('api.update.success')) ;
        }else{
            return responseJson(0,trans('api.old_password.confirmed')) ;
        }

    }

    //change email
    public function changeEmail(Request $request){
        // 1. check if customer
        $user='';
        $user = Auth::guard('customer')->user();

        if (!$user) {
            // 2. check if photographer
            $user = Auth::guard('photographer')->user();
        }

        if (!$user) {
            return responseJson(401, 'not authenticated');
        }

        $rules = [
            'email' => 'required|email',
            'old_password' => 'required',

        ];

        $messages= [
            'email.required' => trans('api.email.required'),
            'email.email' => trans('api.email.invalid'),
            'old_password.required' => trans('api.old_password.required'),
        ];

        $validation = validator()->make($request->all(), $rules,$messages);

        if ($validation->fails()) {
            return responseJson(0,$validation->errors()->first()) ;

        }
        if (Hash::check($request->old_password,$user->password)){
            $user->email=$request->email;
            $user->save();
            return responseJson(1,trans('api.update.success')) ;
        }else{
            return responseJson(1,trans('api.old_password.confirmed')) ;
        }

    }

    //Show & Edit Photographer profile
    public function photographerProfile(Request $request)
    {

        $user = $request->user();


        if ($request->has('name') && $request->name != NULL) {
            $user->update($request->only('name'));
        }

        if ($request->has('gender') && $request->gender != NULL) {
            $user->update($request->only('gender'));
        }
        if ($request->has('mobile' )&& $request->mobile != NULL) {
            $user->update($request->only('mobile'));
        }
        if ($request->has('city_id') && $request->city_id != NULL) {
            $user->update($request->only('city_id'));
        }
        if ($request->has('notification_state')) {
            $state = 1;
            if ($request->notification_state == 'false' || $request->notification_state == '0') {
                $state = 0;
            }
            $user->update(['notification_state' => $state]);
        }
        if ($request->has('description') && $request->description != NULL) {
            $user->update($request->only('description'));
        }
        if ($request->has('from_year') && $request->from_year != NULL) {
            $user->update($request->only('from_year'));
        }

        if ($request->has('categories')) {
            $categories = $request->categories;

            $user->categories()->sync($categories);
        }

        if ($request->hasFile('image') && $request->image != NULL)
        {
            $image = $request->file('image');
            $destinationPath = base_path() . '/uploads';
            $extension = $image->getClientOriginalExtension(); // getting image extension
            $name = time() . '' . rand(11111, 99999) . '.' . $extension; // renameing image
            $image->move($destinationPath, $name); // uploading file to given
            $user->update(['image' => 'uploads/' . $name]);
            //fitImage('uploads/' . $name,200);
        }

        if ($request->hasFile('cover_image') && $request->cover_image != NULL)
        {
            $image = $request->file('cover_image');
            $destinationPath = base_path() . '/uploads';
            $extension = $image->getClientOriginalExtension(); // getting image extension
            $name = time() . '' . rand(11111, 99999) . '.' . $extension; // renameing image
            $image->move($destinationPath, $name); // uploading file to given
            $user->update(['cover_image' => 'uploads/' . $name]);
            //fitImage('uploads/' . $name,200);
        }

        $data = [
            'user' => Photographer::with('categories')->find($user->id),

        ];

        return responseJson(1,trans('api.update.success'),$data);
    }

    // Edit Photographer notification state
    public function photographer_change_notification_state(Request $request)
    {

        $rules = [
            'notification_state' => 'required',
        ];

        $messages= [
            'notification_state.required' => trans('api.notification_state.required'),
        ];

        $validation = validator()->make($request->all(), $rules, $messages);

        if ($validation->fails()) {
            return responseJson(0,$validation->errors()->first()) ;
        }

        $user = $request->user();

        if ($request->has('notification_state')) {
            $state = 1;
            if ($request->notification_state == 'false' || $request->notification_state == '0') {
                $state = 0;
            }
            $user->update(['notification_state' => $state]);
        }

        $data = [
            'user' => Photographer::find($user->id)
        ];

        return responseJson(1,trans('api.update.success'),$data);

    }

    // register token
    public function registerToken(Request $request)
    {

        // 1. check if customer
        $user = Auth::guard('customer')->user();

        if (!$user) {
            // 2. check if photographer
            $user = Auth::guard('photographer')->user();
        }

        if (!$user) {
            return responseJson(401, 'not authenticated');
        }

        $rules = [
            'device_type' => 'required|in:android,ios',
            'token' => 'required',
        ];

        $messages= [
            'device_type.required' => trans('api.device_type.required'),
            'token.required' => trans('api.token.required'),
        ];

        $validation = validator()->make($request->all(), $rules,$messages);

        if ($validation->fails()) {
            return responseJson(0,$validation->errors()->first()) ;
        }

        Token::where('token',$request->token)->delete();
        $user->tokens()->create($request->all());

        return responseJson(1,trans('api.add.success')) ;
    }

    // remove token
    public function removeToken(Request $request)
    {

        // 1. check if customer
        $user = Auth::guard('customer')->user();

        if (!$user) {
            // 2. check if photographer
            $user = Auth::guard('photographer')->user();
        }

        if (!$user) {
            return responseJson(401, 'not authenticated');
        }

        $rules = [
            'device_type' => 'required|in:android,ios',
            'token' => 'required',
        ];

        $messages= [
            'device_type.required' => trans('api.device_type.required'),
            'token.required' => trans('api.token.required'),
        ];

        $validation = validator()->make($request->all(), $rules,$messages);

        if ($validation->fails()) {
            return responseJson(0,$validation->errors()->first()) ;
        }

        Token::where('token',$request->token)->delete();

        return responseJson(1,trans('api.delete.success')) ;
    }

    // Customer confirm reservation
    public function customerConfirmReservation(Request $request)
    {

        $rules = [
            'reservation_id' => 'required',
        ];

        $messages= [
            'reservation_id.required' => trans('api.reservation_id.required'),
        ];

        $validation = validator()->make($request->all(), $rules,$messages);

        if ($validation->fails()) {
            return responseJson(0,$validation->errors()->first()) ;
        }

        $reservation=Reservation::where('status','waitingexecute')->where('rejected',FALSE)->find($request->reservation_id);

        if ($reservation) {
            if($reservation->photographer_ok==FALSE){
                $OK = $reservation->update(array('customer_ok'=>TRUE));
            }else{
                $OK = $reservation->update(array('customer_ok'=>TRUE,'status'=>'done'));
                $aboutapp=Aboutapp::first();
                //save transaction
                $transaction=[];
                $transaction['reservation_id']=$reservation->id;
                $transaction['percent']=$aboutapp->percent;
                $transaction['amount']=$reservation->price;
                $transaction['app_amount']=$reservation->price*($aboutapp->percent/100);
                $transaction['photographer_amount']=$transaction['amount']-$transaction['app_amount'];
                Transaction::create($transaction);
            }
            if ($OK) {
                //send notification to photographer
                //get user token
                $tokens=$reservation->photographer->tokens()->pluck('token')->toArray();
                $customerName=optional($reservation->customer)->name;
                //dd($reservation->service);
                $serviceName=optional($reservation->service)->name;
                //dd($tokens);
                $to = ['include_player_ids' => $tokens];
                $msg_ar='قام '.$customerName.' بتأكيد الانتهاء من خدمة '.$serviceName.'';
                $msg_en=$customerName.' confirm finished service '.$serviceName;
                $notification = [
                    'en' => $msg_en,
                    'ar' => $msg_ar,
                ];
                //dd($notification);
                oneSignalNotification($to , $notification , [
                    'user_type' => 'photographer',
                    'action' => 'confirm finish service',
                    'reservation_id' => $reservation->id,
                ]);
                //save notification
                $noti= [];
                $noti['photographer_id'] = $reservation->photographer_id;
                $noti['msg_ar'] = $msg_ar;
                $noti['msg_en'] = $msg_en;
                $noti['object_id'] = $reservation->id;
                $noti['object_type'] = 'Confirm finish service';
                Notification::create($noti);


                return responseJson(1,trans('api.confirm.success')) ;
            } else {
                return responseJson(0,trans('api.confirm.error')) ;

            }
        } else {
            return responseJson(0,trans('api.confirm.error')) ;
        }
    }

    // Photographer services
    public function photographerServices(Request $request)
    {
        $photographer = Service::with('category')->enabled()->where('photographer_id',$request->id)->latest()->paginate(15);;

        return responseJson(1,trans('api.back.success'),$photographer) ;
    }

    // show services - photographer
    public function services(Request $request)
    {
        $services = $request->user()->services()->with('category')->enabled()->orderBy('created_at','asc')->paginate(30);
        if ($services){
            return responseJson(1,trans('api.back.success'),$services) ;
        }else{
            return responseJson(1,trans('api.back.error'),$services) ;
        }

    }

    // Add service - photographer
    public function addService(Request $request)
    {

        $rules = [
            'name' => 'required',
            'category_id' => 'required',
            'price' => 'required',
            'amount' => 'required',
            'price_for_edit' => 'required',
            'basic_hours' => 'required',
            'price_for_additional_hour' => 'required'

        ];

        $messages= [
            'name.required' => trans('api.name.required'),
            'category_id.required' => trans('api.category.required'),
            'price.required' => trans('api.price.required'),
            'amount.required' => trans('api.amount.required'),
            'price_for_edit.required' => trans('api.price_for_edit.required'),
            'basic_hours.required' => trans('api.basic_hours.required'),
            'price_for_additional_hour.required' => trans('api.price_for_additional_hour.required'),
        ];

        $validation = validator()->make($request->all(), $rules,$messages);

        if ($validation->fails()) {
            return responseJson(0,$validation->errors()->first()) ;

        }
        $request->merge(array('photographer_id' => auth::guard('photographer')->user()->id));
        $service = Service::create($request->all());
        if ($service) {

            return responseJson(1,trans('api.service.success'),$service) ;
        } else {
            return responseJson(0,trans('api.service.error')) ;

        }
    }

    //Edit service
    public function editService(Request $request)
    {
        $rules = [
            'service_id' => 'required',
        ];

        $messages= [
            'service_id.required' => trans('api.service.required'),
        ];

        $validation = validator()->make($request->all(), $rules,$messages);

        if ($validation->fails()) {
            return responseJson(0,$validation->errors()->first()) ;

        }

        $service= Service::find($request->service_id);

        if ($request->has('name')) {
            $service->update($request->only('name'));
        }
        if ($request->has('category_id')) {
            $service->update($request->only('category_id'));
        }
        if ($request->has('price')) {
            $service->update($request->only('price'));
        }
        if ($request->has('amount')) {
            $service->update($request->only('amount'));
        }
        if ($request->has('price_for_edit')) {
            $service->update($request->only('price_for_edit'));
        }
        if ($request->has('basic_hours')) {
            $service->update($request->only('basic_hours'));
        }
        if ($request->has('price_for_additional_hour')) {
            $service->update($request->only('price_for_additional_hour'));
        }
        return responseJson(1,trans('api.update.success'));
    }

    //delete service
    public function deleteService(Request $request)
    {
        $rules = [
            'service_id' => 'required',
        ];

        $messages= [
            'service_id.required' => trans('api.service.required'),
        ];

        $validation = validator()->make($request->all(), $rules,$messages);

        if ($validation->fails()) {
            return responseJson(0,$validation->errors()->first()) ;

        }

        $service= Service::find($request->service_id);
        $service->delete();
        return responseJson(1,trans('api.delete.success'));
    }

    // photographer: my portfolios
    public function my_portfolios()
    {
        $photographer = Auth::guard('photographer')->user();
        $categories = $photographer->portfolios_by_categories;

        /*$portfolio = Category::with('portfolios')->whereHas('portfolios',function ($q) {
            $q->where('photographer_id',auth::guard('photographer')->user()->id);
        })->get();*/

        return responseJson(1,trans('api.back.success'),$categories) ;
    }

    //add portfolios
    public function add_portfolios(Request $request)
    {

        $user = Auth::guard('photographer')->user();

        $rules = [
            'category_id' => 'required',
            //'images.*' => 'required',
            'images' => 'required',
        ];

        $messages= [
            'category_id.required' => trans('api.category.required'),
            'images.required' => trans('api.images.required'),
            'images.array' => trans('api.images.array'),
        ];

        $validation = validator()->make($request->all(), $rules,$messages);

        if ($validation->fails()) {
            return responseJson(0,$validation->errors()->first()) ;
        }

        if (is_string($request->images)) {
            $json_string   = $request->images;
            $my_array_data = json_decode($json_string, TRUE);
            $request->merge(['images' => $my_array_data]);
        }

        if ($request->has('images')) {
            foreach ($request->images as $image) {

                if (is_file($image)) {
                    //$image = $request->file('image');
                    $destinationPath = base_path() . '/uploads';
                    $extension = $image->getClientOriginalExtension(); // getting image extension
                    $name = time() . '' . rand(11111, 99999) . '.' . $extension; // renameing image
                    $image->move($destinationPath, $name); // uploading file to given

                    $image_name = "uploads/" . $name;
                    $user->portfolios()->create(['image' => $image_name, 'category_id' => $request->category_id]);
                }
                else {
                    //get the base-64 from data
                    $base64_str = substr($image, strpos($image, ",") + 1);

                    //decode base64 string
                    $final_image = base64_decode($base64_str);

                    $destinationPath = base_path() . '/uploads/';
                    $pos = strpos($image, ';');
                    $type = explode(':', substr($image, 0, $pos))[1];
                    $extension = explode('/', $type)[1];
                    $name = time() . '' . rand(11111, 99999) . '.' . $extension;
                    //$final_image->move($destinationPath, $name); // uploading file to given

                    file_put_contents($destinationPath . $name, $final_image);

                    $image_name = "uploads/" . $name;
                    $user->portfolios()->create(['image' => $image_name, 'category_id' => $request->category_id]);

                }

            }
        }

        return responseJson(1,trans('api.save.success'));

    }

    // add 1 portfolio
    public function add_portfolio(Request $request)
    {

        $user = Auth::guard('photographer')->user();

        $rules = [
            'category_id' => 'required',
            'image' => 'required',
        ];

        $messages= [
            'category_id.required' => trans('api.category.required'),
            'image.required' => trans('api.images.required'),
        ];

        $validation = validator()->make($request->all(), $rules,$messages);

        if ($validation->fails()) {
            return responseJson(0,$validation->errors()->first()) ;
        }

        if ($request->hasFile('image')) {

            $image = $request->file('image');
            $destinationPath = base_path() . '/uploads';
            $extension = $image->getClientOriginalExtension(); // getting image extension
            $name = time() . '' . rand(11111, 99999) . '.' . $extension; // renameing image
            $image->move($destinationPath, $name); // uploading file to given

            $image_name = "uploads/" . $name;
            $user->portfolios()->create(['image' => $image_name, 'category_id' => $request->category_id]);

        } else {

            $image = $request->image;
            //get the base-64 from data
            $base64_str = substr($image, strpos($image, ",") + 1);

            //decode base64 string
            $final_image = base64_decode($base64_str);

            $destinationPath = base_path() . '/uploads/';
            $pos = strpos($image, ';');
            $type = explode(':', substr($image, 0, $pos))[1];
            $extension = explode('/', $type)[1];
            $name = time() . '' . rand(11111, 99999) . '.' . $extension;
            //$final_image->move($destinationPath, $name); // uploading file to given

            file_put_contents($destinationPath . $name, $final_image);

            $image_name = "uploads/" . $name;
            $user->portfolios()->create(['image' => $image_name, 'category_id' => $request->category_id]);

        }

        return responseJson(1,trans('api.save.success'));

    }

    // Photographer confirm reservation
    public function photographerConfirmReservation(Request $request)
    {

        $rules = [
            'reservation_id' => 'required',
        ];

        $messages= [
            'reservation_id.required' => trans('api.reservation_id.required'),
        ];

        $validation = validator()->make($request->all(), $rules,$messages);

        if ($validation->fails()) {
            return responseJson(0,$validation->errors()->first()) ;

        }

        $reservation=Reservation::where('status','waitingexecute')->where('rejected',FALSE)->find($request->reservation_id);
        if ($reservation) {
            if($reservation->customer_ok==FALSE){
                $OK = $reservation->update(array('photographer_ok'=>TRUE));
            }else{
                $OK = $reservation->update(array('photographer_ok'=>TRUE,'status'=>'done'));
                $aboutapp=Aboutapp::first();
                //save transaction
                $transaction=[];
                $transaction['reservation_id']=$reservation->id;
                $transaction['percent']=$aboutapp->percent;
                $transaction['amount']=$reservation->price;
                $transaction['app_amount']=$reservation->price*($aboutapp->percent/100);
                $transaction['photographer_amount']=$transaction['amount']-$transaction['app_amount'];
                Transaction::create($transaction);
            }
            if ($OK) {
                //send notification to photographer
                //get user token
                $tokens=$reservation->customer->tokens()->pluck('token')->toArray();
                $photographerName=$reservation->photographer->name;
                $serviceName=$reservation->service->name;
                //dd($tokens);
                $to = ['include_player_ids' => $tokens];
                $msg_ar='قام '.$photographerName.' بتأكيد الانتهاء من خدمة '.$serviceName.'';
                $msg_en=$photographerName.' confirm finished service '.$serviceName;
                $notification = [
                    'en' => $msg_en,
                    'ar' => $msg_ar,
                ];
                //dd($notification);
                oneSignalNotification($to , $notification , [
                    'user_type' => 'customer',
                    'action' => 'confirm finish service',
                    'reservation_id' => $reservation->id,
                ]);
                //save notification
                $noti= [];
                $noti['customer_id'] = $reservation->customer_id;
                $noti['msg_ar'] = $msg_ar;
                $noti['msg_en'] = $msg_en;
                $noti['object_id'] = $reservation->id;
                $noti['object_type'] = 'Confirm finish service';
                Notification::create($noti);
                return responseJson(1,trans('api.confirm.success')) ;
            } else {
                return responseJson(0,trans('api.confirm.error')) ;

            }
        } else {
            return responseJson(0,trans('api.confirm.error')) ;
        }
    }

    // Photographer -accept reservation
    public function photographerAcceptReservation(Request $request)
    {

        $rules = [
            'reservation_id' => 'required',
            'accept' => 'required',
        ];

        $messages= [
            'reservation_id.required' => trans('api.reservation_id.required'),
            'accept.required' => trans('api.accept.required'),
        ];

        $validation = validator()->make($request->all(), $rules,$messages);

        if ($validation->fails()) {
            return responseJson(0,$validation->errors()->first()) ;

        }

        $reservation=Reservation::where('status','waitingapproval')->where('rejected',FALSE)->find($request->reservation_id);
        if ($reservation) {
            $OK='';
            if($request->accept==TRUE){
                $OK = $reservation->update(['status'=>'waitingexecute','rejected'=>False]);

                //send notification to photographer
                //get user token
                $tokens=$reservation->customer->tokens()->pluck('token')->toArray();
                $photographerName=$reservation->photographer->name;
                $serviceName=$reservation->service->name;
                //dd($tokens);
                $to = ['include_player_ids' => $tokens];
                $msg_ar='قام '.$photographerName.' بقبول خدمة '.$serviceName.'';
                $msg_en=$photographerName.' accept service '.$serviceName;
                $notification = [
                    'en' => $msg_en,
                    'ar' => $msg_ar,
                ];
                //dd($notification);
                oneSignalNotification($to , $notification , [
                    'user_type' => 'customer',
                    'action' => 'accept service',
                    'reservation_id' => $reservation->id,
                ]);
                //save notification
                $noti= [];
                $noti['customer_id'] = $reservation->customer_id;
                $noti['msg_ar'] = $msg_ar;
                $noti['msg_en'] = $msg_en;
                $noti['object_id'] = $reservation->id;
                $noti['object_type'] = 'Accept service';
                Notification::create($noti);
            }elseif($request->accept==FALSE){
                $OK = $reservation->update(['status'=>'done','rejected'=>TRUE]);

                //send notification to photographer
                //get user token
                $tokens=$reservation->customer->tokens()->pluck('token')->toArray();
                $photographerName=$reservation->photographer->name;
                $serviceName=$reservation->service->name;
                //dd($tokens);
                $to = ['include_player_ids' => $tokens];
                $msg_ar='قام '.$photographerName.' برفض خدمة '.$serviceName.'';
                $msg_en=$photographerName.' reject service '.$serviceName;
                $notification = [
                    'en' => $msg_en,
                    'ar' => $msg_ar,
                ];
                //dd($notification);
                oneSignalNotification($to , $notification , [
                    'user_type' => 'customer',
                    'action' => 'reject service',
                    'reservation_id' => $reservation->id,
                ]);
                //save notification
                $noti= [];
                $noti['customer_id'] = $reservation->customer_id;
                $noti['msg_ar'] = $msg_ar;
                $noti['msg_en'] = $msg_en;
                $noti['object_id'] = $reservation->id;
                $noti['object_type'] = 'Accept service';
                Notification::create($noti);
            }
            if ($OK) {
                return responseJson(1,trans('api.confirm.success')) ;
            } else {
                return responseJson(0,trans('api.confirm.error')) ;
            }
        } else {
            return responseJson(0,trans('api.confirm.error')) ;
        }
    }

    // Add reservation
    public function addReservation(Request $request)
    {

        $rules = [
            'service_id' => 'required',
            'date' => 'required',
            'time' => 'required',
            'ampm' => 'required'
        ];

        $messages= [
            'service_id.required' => trans('api.service.required'),
            'date.required' => trans('api.date.required'),
            'ampm.required' => trans('api.ampm.required'),
        ];

        $validation = validator()->make($request->all(), $rules,$messages);

        if ($validation->fails()) {
            return responseJson(0,$validation->errors()->first()) ;

        }

        $service=Service::find($request->service_id);
        if ($service) {
            $request->merge(array('status' => 'waitingapproval'));
            $request->merge(array('customer_id' => auth::guard('customer')->user()->id));
            $request->merge(array('photographer_id' => $service->photographer_id));
            $request->merge(array('price' => $service->price));
            $request->merge(array('amount' => $service->amount));
            $request->merge(array('price_for_edit' => $service->price_for_edit));
            $request->merge(array('basic_hours' => $service->basic_hours));
            $request->merge(array('price_for_additional_hour' => $service->price_for_additional_hour));
            $request->merge(array('pay_method' => 'cash'));
            $all_date=date('Y-m-d H:i:s', strtotime($request->date. ' ' . $request->time. ' ' . $request->ampm));
            $request->merge(array('datetime' => $all_date));
            $reservation = Reservation::create($request->all());

            if ($reservation) {
                //send notification to photographer
                //get user token
                $tokens=$reservation->photographer->tokens()->pluck('token')->toArray();
                $customerName=$reservation->customer->name;
                $serviceName=$reservation->service->name;
                //dd($tokens);
                $to = ['include_player_ids' => $tokens];
                $msg_ar='قام '.$customerName.' باضافة حجز جديد لخدمة '.$serviceName.'';
                $msg_en=$customerName.' booked you service '.$serviceName;
                $notification = [
                    'en' => $msg_en,
                    'ar' => $msg_ar,
                ];
                //dd($notification);
                oneSignalNotification($to , $notification , [
                    'user_type' => 'photographer',
                    'action' => 'New reservation',
                    'reservation_id' => $reservation->id,
                ]);
                //save notification
                $noti= [];
                $noti['photographer_id'] = $reservation->photographer_id;
                $noti['msg_ar'] = $msg_ar;
                $noti['msg_en'] = $msg_en;
                $noti['object_id'] = $reservation->id;
                $noti['object_type'] = 'New reservation';
                Notification::create($noti);

                return responseJson(1,trans('api.service.success'),$reservation) ;
            } else {
                return responseJson(0,trans('api.service.error')) ;

            }
        } else {
            return responseJson(0,trans('api.service.error')) ;
        }
    }

    //customer  edit reservation
    public function editReservation(Request $request)
    {

        $rules = [
            'reservation_id' => 'required',
            'date' => 'required',
            'time' => 'required',
            'ampm' => 'required'
        ];

        $messages = [
            'reservation_id.required' => trans('api.reservation_id.required'),
        ];

        $validation = validator()->make($request->all(), $rules, $messages);

        if ($validation->fails()) {
            return responseJson(0, $validation->errors()->first());
        }

        $reservation = Reservation::where('status', 'waitingapproval')->find($request->reservation_id);

        if ($reservation) {

            $all_date = date('Y-m-d H:i:s', strtotime($request->date . ' ' . $request->time . ' ' . $request->ampm));
            $updated = $reservation->update(['datetime' => $all_date]);

            if ($updated) {

                //send notification to photographer
                //get user token
                $tokens = $reservation->photographer->tokens()->pluck('token')->toArray();
                $customerName = $reservation->customer->name;
                $serviceName = $reservation->service->name;
                //dd($tokens);
                $to = ['include_player_ids' => $tokens];
                $msg_ar = 'قام ' . $customerName . ' بتعديل حجز ' . $serviceName . '';
                $msg_en = $customerName . ' edit reservation ' . $serviceName;
                $notification = [
                    'en' => $msg_en,
                    'ar' => $msg_ar,
                ];
                //dd($notification);
                oneSignalNotification($to, $notification, [
                    'user_type' => 'photographer',
                    'action' => 'edit reservation',
                    'reservation_id' => $reservation->id,
                ]);
                //save notification
                $noti = [];
                $noti['photographer_id'] = $reservation->photographer_id;
                $noti['msg_ar'] = $msg_ar;
                $noti['msg_en'] = $msg_en;
                $noti['object_id'] = $reservation->id;
                $noti['object_type'] = 'Confirm finish service';
                Notification::create($noti);

                return responseJson(1, trans('api.service.success'), $reservation);

            } else {
                return responseJson(0, trans('api.service.error'));
            }

        }

        return responseJson(0, trans('api.service.error'));

    }

    //customer  delete reservation
    public function deleteReservation(Request $request)
    {

        $rules = [
            'reservation_id' => 'required'
        ];

        $messages= [
            'reservation_id.required' => trans('api.reservation_id.required')
        ];

        $validation = validator()->make($request->all(), $rules,$messages);

        if ($validation->fails()) {
            return responseJson(0,$validation->errors()->first()) ;

        }
        $reservation = Reservation::where('status','waitingapproval')->find($request->reservation_id);
        //dd($reservation);
        if($reservation){
            $reservation=Reservation::find($request->reservation_id);

            //send notification to photographer
            //get user token
            $tokens=$reservation->photographer->tokens()->pluck('token')->toArray();
            $customerName=$reservation->customer->name;
            $serviceName=$reservation->service->name;
            //dd($tokens);
            $to = ['include_player_ids' => $tokens];
            $msg_ar='قام '.$customerName.' بحذف حجز '.$serviceName.'';
            $msg_en=$customerName.' delete reservation '.$serviceName;
            $notification = [
                'en' => $msg_en,
                'ar' => $msg_ar,
            ];
            //dd($notification);
            oneSignalNotification($to , $notification , [
                'user_type' => 'photographer',
                'action' => 'edit reservation',
                'reservation_id' => $reservation->id,
            ]);
            //save notification
            $noti= [];
            $noti['photographer_id'] = $reservation->photographer_id;
            $noti['msg_ar'] = $msg_ar;
            $noti['msg_en'] = $msg_en;
            $noti['object_id'] = $reservation->id;
            $noti['object_type'] = 'Confirm finish service';
            Notification::create($noti);
            Reservation::find($request->reservation_id)->delete();
            return responseJson(1,trans('api.delete.success')) ;
        } else {
            return responseJson(0,trans('api.delete.error')) ;
        }


    }

//customer  delete portfolio
    public function deletePortfolio(Request $request)
    {

        $rules = [
            'portfolio_id' => 'required'
        ];

        $messages= [
            'portfolio_id.required' => trans('api.portfolio.required')
        ];

        $validation = validator()->make($request->all(), $rules,$messages);

        if ($validation->fails()) {
            return responseJson(0,$validation->errors()->first()) ;

        }
        $portfolio=Portfolio::find($request->portfolio_id);
        $portfolio->delete();
        if($portfolio){
            return responseJson(1,trans('api.delete.success')) ;
        } else {
            return responseJson(0,trans('api.delete.error')) ;
        }


    }

// show waiting approval reservation count for photographer/customer
    public function reservations_count() {

        $photographer = Auth::guard('photographer')->user();
        $customer = Auth::guard('customer')->user();

        $count = 0;
        if ($photographer != null) {
            $count = $photographer->reservations()->where('status','waitingapproval')->count();
        } else if ($customer != null) {
            $count = $customer->reservations()->where('status', '!=', 'done')->count();
        } else {
            return responseJson(403,'not_authenticated') ;
        }

        return responseJson(1,trans('api.back.success'),$count) ;

    }

// get notifications count for photographer/customer
    public function notifications_count() {
        // 1. check if customer
        $user = Auth::guard('customer')->user();
        if (!$user) {
            // 2. check if photographer
            $user = Auth::guard('photographer')->user();
        }

        $count = $user->notifications()->where('seen', 0)->count();
        return responseJson(1,trans('api.back.success'),$count) ;
    }

// get notifications
    public function notifications(Request $request){
        // 1. check if customer
        $user = Auth::guard('customer')->user();
        if (!$user) {
            // 2. check if photographer
            $user = Auth::guard('photographer')->user();
        }

        if (!$user) {
            return responseJson(401, 'not authenticated');
        }

        $notifications = $user->notifications()->paginate(15);
        $count = $user->notifications()->where('seen', 0)->count();

        return responseJson(1,trans('api.add.success'),['unseen_count'=>$count,'notifications'=>$notifications]) ;
    }

    // get photographer profits
    public function photographerProfits(Request $request){

        $transactions =$request->user()->transactions()->paginate(20);
        $amount =$request->user()->transactions()->sum('transactions.amount');
        $photographer_amount =$request->user()->transactions()->sum('transactions.photographer_amount');
        $app_amount =$request->user()->transactions()->sum('transactions.app_amount');
        //return($request->user()->id);

        return responseJson(1,trans('api.back.success'),['all_amount'=>$amount,'photographer_amount'=>$photographer_amount,'app_amount'=>$app_amount,'transactions'=>$transactions]) ;

    }


}
