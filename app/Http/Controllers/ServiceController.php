<?php 

namespace App\Http\Controllers;

use App\Models\Service;
use Illuminate\Http\Request;

class ServiceController extends Controller
{

  /**
   * Display a listing of the resource.
   *
   * @return Response
   */
  public function index(Request $request)
  {
      $services = Service::where(function($query) use($request){

          if($request->has('photographer_id'))
          {
              if($request->photographer_id!='0'){
                  $query->where('photographer_id',$request->photographer_id);
              }

          }


          if($request->has('name'))
          {
              if($request->name!=''||$request->name!=NULL){
                  $query->where('name','LIKE','%'.$request->name.'%');
              }
          }

          if($request->has('category_id'))
          {
              if($request->category_id!='0'){
                  $query->where('category_id',$request->category_id);
              }

          }

          if($request->has('from'))
          {
              if($request->from!=''||$request->from!=NULL){
                  $query->where('price','>=',$request->from);
              }
          }

          if($request->has('to'))
          {
              if($request->to!=''||$request->to!=NULL){
                  $query->where('price','<=',$request->to);
              }
          }





      })->orderBy('created_at', 'desc')->paginate(12)->appends($request->all());
      return view('dashboard.services.index',compact('services'));
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return Response
   */
  public function create()
  {
    
  }

  /**
   * Store a newly created resource in storage.
   *
   * @return Response
   */
  public function store()
  {
    
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function show($id)
  {
    
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function edit($id)
  {
    
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function update($id)
  {
    
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function destroy($id)
  {
    
  }

    public function enabled($id)
    {
        $model = Service::findOrFail($id);
        $model->enabled = 1;
        $model->save();
        flash()->success('تم التفعيل');
        return back();
    }

    public function disabled($id)
    {
        $model = Service::findOrFail($id);
        $model->enabled = 0;
        $model->save();
        flash()->success('تم إلغاء التفعيل');
        return back();
    }

}

?>