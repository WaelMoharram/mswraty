<?php
//Json array response
function responseJson($status,$msg,$data = NULL){
$response = [
'status' => $status,
'message' => $msg,
'data' => $data
];
return response()->json($response);
}

// one signal notification code
function oneSignalNotification($audience = ['included_segments' => array('All')], $contents = ['en' => ''], $data = [])
{

    // audience include_player_ids
    $appId = ['app_id' => env('ONE_SIGNAL_APP_ID')];

    $fields = json_encode((array)$appId + (array)$audience + ['contents' => (array)$contents] + ['data' => (array)$data] + ['ios_badgeType' => 'Increase', 'ios_badgeCount' => 1] + ['headings' => ['en' => 'Mswrati مصوراتي']]);
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
        'Authorization: Basic ' . env('ONE_SIGNAL_KEY')));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($ch, CURLOPT_HEADER, FALSE);
    curl_setopt($ch, CURLOPT_POST, TRUE);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

    $response = curl_exec($ch);
    curl_close($ch);

    return $response;
}

