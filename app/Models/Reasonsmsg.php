<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Reasonsmsg extends Model 
{

    protected $table = 'reason_for_contact';
    public $timestamps = true;

    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $fillable = array('name_ar', 'name_en');
    protected $appends = ['name'];
    protected $hidden = ['name_ar', 'name_en', 'created_at', 'deleted_at', 'updated_at'];

    public function contacts()
    {
        return $this->hasMany('App\Models\Contactus');
    }

    public function getNameAttribute() {
        if (request()->header('Accept-Language') == 'ar') {
            return $this->name_ar;
        }

        return $this->name_en;
    }



}