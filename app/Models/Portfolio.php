<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Portfolio extends Model 
{

    protected $table = 'portfolios';
    public $timestamps = true;

    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $fillable = array('photographer_id', 'category_id', 'image');
    protected $hidden = ['created_at', 'deleted_at', 'updated_at'];

    protected $casts = [
        'photographer_id' => 'integer',
        'category_id' => 'integer',
    ];

    public function photographer()
    {
        return $this->belongsTo('App\Models\Photographer');
    }

    public function category()
    {
        return $this->belongsTo('App\Models\Category');
    }

}