<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Contactus extends Model 
{

    protected $table = 'contact_us';
    public $timestamps = true;

    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $fillable = array('email', 'reason_for_contact_id', 'message');

    public function reasonmsg()
    {
        return $this->belongsTo('App\Models\Reasonsmsg','reason_for_contact_id');
    }



}