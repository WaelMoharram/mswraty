<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Notification extends Model 
{

    protected $table = 'notifications';
    public $timestamps = true;
    protected $fillable = array('customer_id', 'photographer_id', 'msg_ar', 'msg_en', 'object_id', 'object_type', 'seen');
    protected $appends = ['msg', 'date_human'];
    protected $hidden = ['msg_ar', 'msg_en','created_at', 'deleted_at', 'updated_at'];

    protected $casts = [
        'id' => 'integer',
        'object_id' => 'integer',
        'seen' => 'boolean',
    ];

    public function getSeenAttribute($value) {
        if ($value == 0) {
            $this->seen = 1;
            $this->save();
        }

        return (boolean)$value;
    }

    public function getDateHumanAttribute() {
        if (request()->header('Accept-Language') == 'ar') {
            Carbon::setLocale('ar');
        }
        return $this->created_at->diffForHumans();
    }

    public function getCustomerIdAttribute($value) {
        if ($value == null) { return 0; }
        return (integer) $value;
    }

    public function getPhotographerIdAttribute($value) {
        if ($value == null) { return 0; }
        return (integer) $value;
    }

    public function getMsgAttribute() {
        if (request()->header('Accept-Language') == 'ar') {
            return $this->msg_ar;
        }

        return $this->msg_en;
    }

}