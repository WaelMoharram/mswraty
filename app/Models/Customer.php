<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Customer extends Model 
{

    protected $table = 'customers';
    public $timestamps = true;

    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $fillable = array('name', 'email', 'password', 'image', 'gender', 'mobile', 'fb_token', 'g_token', 'city_id', 'notification_state', 'api_token', 'enabled','reset_code');
    protected $hidden = ['password', 'fb_token', 'g_token', 'created_at', 'deleted_at', 'updated_at', 'api_token', 'enabled','reset_code'];
    protected $with = ['city'];

    protected $casts = [
        'id' => 'integer',
        'city_id' => 'integer',
        'notification_state' => 'boolean',
    ];

    public function city()
    {
        return $this->belongsTo('App\Models\City');
    }

    public function tokens()
    {
        return $this->morphMany('App\Models\Token', 'tokenable');
    }

    public function reservations()
    {
        return $this->hasMany('App\Models\Reservation');
    }

    public function notifications()
    {
        return $this->hasMany('App\Models\Notification', 'customer_id')->latest();
    }

    public function scopeEnabled($q) {
        $q->where('enabled', 1);
    }

    public function getImageAttribute($value) {
        if ($value == null) { return ""; }
        return (string) $value;
    }

    public function getMobileAttribute($value) {
        if ($value == null) { return ""; }
        return (string) $value;
    }

    public function getGenderAttribute($value) {
        if ($value == null) { return ""; }
        return (string) $value;
    }
    public function getCityIdAttribute($value) {
        if ($value == null) { return 0; }
        return (integer) $value;
    }

}