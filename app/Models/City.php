<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class City extends Model 
{

    protected $table = 'cities';
    public $timestamps = true;

    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $fillable = array('country_id', 'name_ar', 'name_en');
    protected $appends = ['name'];
    protected $hidden = ['name_ar', 'name_en', 'enabled', 'created_at', 'deleted_at', 'updated_at'];

    protected $casts = [
        'id' => 'integer',
        'country_id' =>'integer',

    ];

    public function country()
    {
        return $this->belongsTo('App\Models\Country');
    }

    public function customers()
    {
        return $this->hasMany('App\Models\Customer');
    }

    public function photographers()
    {
        return $this->hasMany('App\Models\Photographer');
    }

    public function getNameAttribute() {
        if (request()->header('Accept-Language') == 'ar') {
            return $this->name_ar;
        }

        return $this->name_en;
    }

    public function scopeEnabled($q) {
        $q->where('enabled', 1);
    }

}