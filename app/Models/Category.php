<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{

    protected $table = 'categories';
    public $timestamps = true;

    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $fillable = array('name_ar', 'name_en','icon');
    protected $appends = ['name'];
    protected $hidden = ['name_ar', 'name_en', 'enabled', 'created_at', 'deleted_at', 'updated_at'];

    // casting attributes to return proper types with json
    protected $casts = [
        'id' => 'integer',
    ];

    public function services()
    {
        return $this->hasMany('App\Models\Service');
    }

    public function portfolios()
    {
        return $this->hasMany('App\Models\Portfolio');
    }

    public function getNameAttribute() {
        if (request()->header('Accept-Language') == 'ar') {
            return $this->name_ar;
        }

        return $this->name_en;
    }

    public function scopeEnabled($q) {
        $q->where('enabled', 1);
    }

    public function getIconAttribute($value) {
        if ($value == null) { return ""; }
        return (string) $value;
    }

}