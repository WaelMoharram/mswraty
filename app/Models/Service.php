<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Service extends Model 
{

    protected $table = 'services';
    public $timestamps = true;

    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $fillable = array('photographer_id', 'category_id', 'name', 'price', 'amount', 'price_for_edit', 'basic_hours', 'price_for_additional_hour');
    protected $hidden = ['photographer_id', 'enabled', 'created_at', 'deleted_at', 'updated_at'];
    protected $casts = [
        'id' => 'integer',
        'category_id' => 'integer',
        'photographer_id' => 'integer',
        'price' => 'real',
        'amount' => 'integer',
        'price_for_edit' => 'real',
        'basic_hours' => 'integer',
        'price_for_additional_hour' => 'real',
    ];
    public function reservations()
    {
        return $this->hasMany('App\Models\Reservation');
    }

    public function photographer()
    {
        return $this->belongsTo('App\Models\Photographer','photographer_id');
    }

    public function category()
    {
        return $this->belongsTo('App\Models\Category','category_id');
    }

    public function scopeEnabled($q) {
        $q->where('enabled', 1);
    }


}