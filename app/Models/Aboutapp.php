<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Aboutapp extends Model 
{

    protected $table = 'about_app';
    public $timestamps = true;
    protected $fillable = array('about_ar', 'about_en', 'phone', 'email', 'website','percent');
    protected $appends = ['about'];
    protected $hidden = ['id', 'about_ar', 'about_en', 'created_at', 'updated_at'];


    protected $casts = [
        'percent' => 'integer'
    ];


    public function reasons()
    {
        return $this->hasMany('App\Models\Reasonsmsg');
    }

    public function getAboutArAttribute($value) {
        if ($value == null) { return ""; }
        return (string) $value;
    }

    public function getAboutEnAttribute($value) {
        if ($value == null) { return ""; }
        return (string) $value;
    }
    public function getPhoneAttribute($value) {
        if ($value == null) { return ""; }
        return (string) $value;
    }
    public function getEmailAttribute($value) {
        if ($value == null) { return ""; }
        return (string) $value;
    }
    public function getWebsiteAttribute($value) {
        if ($value == null) { return ""; }
        return (string) $value;
    }

    public function getAboutAttribute() {
        if (request()->header('Accept-Language') == 'ar') {
            return $this->about_ar;
        }

        return $this->about_en;
    }




}