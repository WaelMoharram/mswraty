<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{

    protected $table = 'transactions';
    public $timestamps = true;
    protected $fillable = array('reservation_id', 'percent', 'amount', 'photographer_amount', 'app_amount');

    protected $appends = ['service_name'];

    public function getServiceNameAttribute() {
        return $this->reservation->service->name;
    }

    public function reservation()
    {
        return $this->belongsTo('App\Models\Reservation','reservation_id');
    }

}
