<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Reservation extends Model 
{

    protected $table = 'reservations';
    public $timestamps = true;

    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $fillable = array('price','amount','price_for_edit','edit_amount','basic_hours','price_for_additional_hour','pay_method','customer_id', 'photographer_id', 'service_id', 'datetime', 'status', 'rate', 'gender', 'photographer_ok', 'customer_ok','rejected');
    protected $hidden = ['created_at', 'deleted_at', 'updated_at'];

    protected $casts = [
        'customer_id' => 'integer',
        'photographer_id' => 'integer',
        'service_id' => 'integer',
        'rate' => 'integer',
        'photographer_ok' => 'boolean',
        'customer_ok' => 'boolean',
        'rejected' => 'boolean',
        'price' => 'real',
        'amount' => 'integer',
        'price_for_edit' => 'real',
        'edit_amount' => 'integer',
        'basic_hours' => 'integer',
        'price_for_additional_hour' => 'real',

    ];

    public function customer()
    {
        return $this->belongsTo('App\Models\Customer','customer_id');
    }

    public function photographer()
    {
        return $this->belongsTo('App\Models\Photographer','photographer_id');
    }

    public function service()
    {
        return $this->belongsTo('App\Models\Service','service_id')->withTrashed();
    }

    public function transaction(){
        return $this->hasOne('App\Models\Transaction');
    }

    public function scopeCustomerOk($q,$customer_ok) {

        $q->where('customer_ok', $customer_ok);

    }
    public function scopePhotographerOk($q,$photographer_ok) {

        $q->where('photographer_ok', $photographer_ok);

    }

}