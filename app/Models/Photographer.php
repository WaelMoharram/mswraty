<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Photographer extends Model 
{

    protected $table = 'photographers';
    public $timestamps = true;

    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $fillable = array('name', 'email', 'password', 'image', 'cover_image', 'mobile', 'fb_token', 'g_token', 'gender', 'city_id', 'description', 'notification_state', 'from_year', 'api_token', 'enabled');
    protected $appends = ['rate','rate_count','min_price','categories_human','subtitle'];
    protected $hidden = ['password', 'fb_token', 'g_token', 'enabled', 'api_token', 'created_at', 'deleted_at', 'updated_at'];
    protected $with = ['city'];

    protected $casts = [
        'id' => 'integer',
        'city_id' => 'integer',
        'notification_state' => 'boolean',
        'min_price' => 'decimal',
    ];

    public function portfolios()
    {
        return $this->hasMany('App\Models\Portfolio');
    }

    public function notifications()
    {
        return $this->hasMany('App\Models\Notification', 'photographer_id')->latest();
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class, 'photographer_category', 'photographer_id', 'category_id')->withTimestamps();
    }

    public function getPortfoliosByCategoriesAttribute()
    {
        return Category::with(['portfolios' => function($q) {
            $q->where('photographer_id',$this->id);
        }])->whereHas('portfolios',function ($q){
            $q->where('photographer_id',$this->id);
        })->get();

        /*return Category::with('portfolios')->whereHas('portfolios',function ($q){
            $q->where('photographer_id',$this->id);
        })->get();*/
    }

    public function city()
    {
        return $this->belongsTo('App\Models\City','city_id');
    }

    public function tokens()
    {
        return $this->morphMany('App\Models\Token', 'tokenable');
    }

    public function reservations()
    {
        return $this->hasMany('App\Models\Reservation');
    }

    public function services()
    {
        return $this->hasMany('App\Models\Service');
    }

    public function transactions()
    {
        return $this->hasManyThrough('App\Models\Transaction', 'App\Models\Reservation');
    }

    public function getRateAttribute() {
        return (integer)$this->reservations()->avg('rate');
    }

    public function getRateCountAttribute() {
        return (integer)$this->reservations()->where('rate','!=',0)->count();
    }

    public function getMinPriceAttribute() {
        //return number_format($this->services()->where('enabled',TRUE)->min('price'), 2);
        return  (real) $this->services()->where('enabled',TRUE)->min('price');
    }

    public function getCategoriesHumanAttribute() {
        if (request()->header('Accept-Language') == 'ar') {
            return implode(",",Category::whereHas('portfolios',function ($q) {
                $q->where('photographer_id',$this->id);
            })->pluck('name_ar')->toArray());
        }

        return implode(",",Category::whereHas('portfolios',function ($q) {
            $q->where('photographer_id',$this->id);
        })->pluck('name_en')->toArray());
    }

    public function getSubtitleAttribute() {

        $gender = $this->gender;
        $isArabic = (request()->header('Accept-Language') == 'ar');

        if ($isArabic) {
            if ($this->gender == 'male') {
                $gender = 'ذكر';
            }
            if ($this->gender == 'female') {
                $gender = 'انثى';
            }
        }

        $char = '';
        if($this->city && $this->gender){
            $char = $isArabic ? '، ' : ', ';
        }

        if($this->city == null){
            return $gender;
        }else{
            return $this->city->name.$char.$gender;
        }

    }

    public function scopeEnabled($q) {
        $q->where('enabled', 1);
    }

    public function scopeCategory($q,$category_id) {
        $q->whereHas('categories', function ($q2) use ($category_id) {
            $q2->where('photographer_category.category_id', $category_id);
        });
    }

    public function getImageAttribute($value) {
        if ($value == null) { return ""; }
        return (string) $value;
    }

    public function getCoverImageAttribute($value) {
        if ($value == null) { return ""; }
        return (string) $value;
    }
    public function getMobileAttribute($value) {
        if ($value == null) { return ""; }
        return (string) $value;
    }

    public function getCityIdAttribute($value) {
        if ($value == null) { return 0; }
        return (integer) $value;
    }

    public function getGenderAttribute($value) {
        if ($value == null) { return ""; }
        return (string) $value;
    }

    public function getDescriptionAttribute($value) {
        if ($value == null) { return ""; }
        return (string) $value;
    }

    public function getFromYearAttribute($value) {
        if ($value == null) { return 0; }
        return (integer) $value;
    }

}