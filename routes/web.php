<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Auth::routes();
Route::get('/', function () {
    return redirect('admin');
});
Route::group(['middleware' => 'auth','prefix' => 'admin'],function (){

    //Route::get('/', function () {
    //    return view('dashboard.home');
    //});
    Route::get('/', 'HomeController@index');
    Route::get('profit_report', 'HomeController@report');
    Route::get('transaction_report/{id}', 'HomeController@photographer_transactions');
    Route::resource('admin','UserController');
    Route::resource('about_app','AboutappController');
    Route::resource('reason','ReasonsmsgController');
    Route::resource('message','ContactusController');
    Route::resource('city','CityController');

    Route::resource('category','CategoryController');
    Route::get('category/{id}/enabled', 'CategoryController@enabled');
    Route::get('category/{id}/disabled', 'CategoryController@disabled');

    Route::resource('customer','CustomerController');
    Route::get('customer/{id}/enabled', 'CustomerController@enabled');
    Route::get('customer/{id}/disabled', 'CustomerController@disabled');

    Route::resource('photographer','PhotographerController');
    Route::get('photographer/{id}/enabled', 'PhotographerController@enabled');
    Route::get('photographer/{id}/disabled', 'PhotographerController@disabled');

    Route::resource('service','ServiceController');
    Route::get('service/{id}/enabled', 'ServiceController@enabled');
    Route::get('service/{id}/disabled', 'ServiceController@disabled');

    Route::resource('reservation','ReservationController');

    Route::get('notification', 'NotificationController@view');
    Route::post('notification', 'NotificationController@send');


});


