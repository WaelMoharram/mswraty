<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

Route::group(['middleware' => 'lang','prefix' => 'v1'],function (){
    Route::group(['prefix' => 'customer'],function (){
        Route::post('register','Api\AuthController@CustomerRegister');
        Route::post('login','Api\AuthController@CustomerLogin');
        Route::post('facebook','Api\AuthController@CustomerFacebook');
        Route::post('google','Api\AuthController@CustomerGoogle');
        Route::post('reset','Api\AuthController@customerReset');

        // under auth
        Route::group(['middleware' => 'auth:customer'],function (){
            Route::post('profile','Api\ApiController@customerProfile');
            Route::post('change_notification_state','Api\ApiController@customer_change_notification_state');
            Route::post('reservations','Api\ApiController@Reservations');
            Route::post('rate','Api\ApiController@Rate');
            Route::post('add_reservation','Api\ApiController@addReservation');
            Route::post('edit_reservation','Api\ApiController@editReservation');
            Route::post('delete_reservation','Api\ApiController@deleteReservation');
            Route::post('confirm_reservation','Api\ApiController@customerConfirmReservation');
        });
    });

    Route::group(['prefix' => 'photographer'],function (){
        Route::post('register','Api\AuthController@photographerRegister');
        Route::post('login','Api\AuthController@photographerLogin');
        Route::post('facebook','Api\AuthController@PhotographerFacebook');
        Route::post('google','Api\AuthController@PhotographerGoogle');
        Route::post('reset','Api\AuthController@photographerReset');
        // under auth
        Route::group(['middleware' => 'auth:photographer'],function (){
            Route::post('profile','Api\ApiController@photographerProfile');
            Route::post('change_notification_state','Api\ApiController@photographer_change_notification_state');
            Route::post('reservations','Api\ApiController@Reservations');
            Route::post('services','Api\ApiController@services');
            Route::post('add_service','Api\ApiController@addService');
            Route::post('edit_service','Api\ApiController@editService');
            Route::post('delete_service','Api\ApiController@deleteService');
            Route::post('portfolios','Api\ApiController@my_portfolios');
            Route::post('add_portfolio','Api\ApiController@add_portfolio');
            Route::post('add_portfolios','Api\ApiController@add_portfolios');
            Route::post('delete_portfolio','Api\ApiController@deletePortfolio');
            Route::post('confirm_reservation','Api\ApiController@photographerConfirmReservation');
            Route::post('accept_reservation','Api\ApiController@photographerAcceptReservation');
            Route::post('transactions','Api\ApiController@photographerProfits');
        });
    });

    Route::get('categories','Api\ApiController@Categories');
    Route::get('cities','Api\ApiController@Cities');
    Route::get('about_app','Api\ApiController@AboutApp');
    Route::get('reasons','Api\ApiController@Reasons');
    Route::post('contact_us','Api\ApiController@ContactUs');
    Route::any('photographers','Api\ApiController@photographers');
    Route::get('photographer','Api\ApiController@photographer');
    Route::get('photographer_services','Api\ApiController@photographerServices');
    Route::get('photographer_portfolio','Api\ApiController@photographerPortfolio');
    Route::post('register_token','Api\ApiController@registerToken');
    Route::post('remove_token','Api\ApiController@removeToken');
    Route::post('change_password','Api\ApiController@changePassword');
    Route::post('change_email','Api\ApiController@changeEmail');

    Route::post('notifications','Api\ApiController@notifications');
    Route::post('notifications_count','Api\ApiController@notifications_count');

    Route::post('reservations_count','Api\ApiController@reservations_count');

});

