<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateReservationsTable extends Migration {

	public function up()
	{
		Schema::create('reservations', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->softDeletes();
			$table->integer('customer_id');
			$table->integer('photographer_id');
			$table->integer('service_id');
			$table->datetime('datetime');
			$table->enum('status', array('waitingapproval', 'waitingexecute', 'done'));
			$table->smallInteger('rate')->default('0');
			$table->tinyInteger('photographer_ok')->default('0');
			$table->tinyInteger('customer_ok')->default('0');
            $table->tinyInteger('rejected')->default('0');
			$table->decimal('price');
			$table->integer('amount');
			$table->decimal('price_for_edit');
			$table->integer('edit_amount')->default('0');;
			$table->integer('basic_hours');
			$table->decimal('price_for_additional_hour');
			$table->string('pay_method')->default('cash');
		});
	}

	public function down()
	{
		Schema::drop('reservations');
	}
}