<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNotificationsTable extends Migration {

	public function up()
	{
		Schema::create('notifications', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->integer('customer_id')->nullable();
			$table->integer('photographer_id')->nullable();
			$table->string('msg_ar');
			$table->string('msg_en');
			$table->integer('object_id')->nullable();
			$table->string('object_type')->nullable();
            $table->boolean('seen')->default(0);
		});
	}

	public function down()
	{
		Schema::drop('notifications');
	}
}