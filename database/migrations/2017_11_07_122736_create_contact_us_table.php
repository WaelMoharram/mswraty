<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateContactUsTable extends Migration {

	public function up()
	{
		Schema::create('contact_us', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->softDeletes();
			$table->string('email');
			$table->integer('reason_for_contact_id');
			$table->longText('message');
		});
	}

	public function down()
	{
		Schema::drop('contact_us');
	}
}