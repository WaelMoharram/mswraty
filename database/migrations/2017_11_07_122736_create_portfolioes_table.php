<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePortfolioesTable extends Migration {

	public function up()
	{
		Schema::create('portfolioes', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->softDeletes();
			$table->integer('photographer_id');
			$table->integer('category_id');
			$table->string('image');
		});
	}

	public function down()
	{
		Schema::drop('portfolioes');
	}
}