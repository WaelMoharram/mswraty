<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAboutAppTable extends Migration {

	public function up()
	{
		Schema::create('about_app', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->string('about_ar');
			$table->string('about_en');
			$table->string('phone');
			$table->string('email');
			$table->string('website');
		});
	}

	public function down()
	{
		Schema::drop('about_app');
	}
}