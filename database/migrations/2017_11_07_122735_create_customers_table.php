<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCustomersTable extends Migration {

	public function up()
	{
		Schema::create('customers', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->softDeletes();
			$table->string('name');
			$table->string('email')->unique();
			$table->string('password')->nullable();
			$table->string('image')->nullable();
			$table->string('mobile')->nullable();
			$table->string('fb_token')->nullable();
			$table->string('g_token')->nullable();
			$table->integer('city_id')->nullable();
			$table->boolean('notification_state')->default(1);
			$table->string('api_token')->nullable();
			$table->enum('gender', array('male', 'female'))->nullable();
            $table->boolean('enabled')->default(1);
            $table->integer('reset_code')->nullable();
		});
	}

	public function down()
	{
		Schema::drop('customers');
	}
}