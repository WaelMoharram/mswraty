<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTransactionsTable extends Migration {

	public function up()
	{
		Schema::create('transactions', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->integer('reservation_id');
			$table->integer('percent');
			$table->decimal('amount');
			$table->decimal('photographer_amount');
			$table->decimal('app_amount');
		});
	}

	public function down()
	{
		Schema::drop('transactions');
	}
}