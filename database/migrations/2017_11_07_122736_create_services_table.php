<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateServicesTable extends Migration {

	public function up()
	{
		Schema::create('services', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->softDeletes();
			$table->integer('photographer_id');
			$table->integer('category_id');
			$table->string('name');
			$table->decimal('price');
			$table->integer('amount');
			$table->decimal('price_for_edit');
			$table->integer('basic_hours');
			$table->decimal('price_for_additional_hour');
			$table->boolean('enabled')->default(1);
		});
	}

	public function down()
	{
		Schema::drop('services');
	}
}