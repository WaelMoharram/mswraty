<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted'             => 'The must be accepted.',
    'name' => [
        'required' => 'Name is required .',
        'unique' => 'Name is taken .'
    ],
    'mobile' => [
        'required' => 'Mobile is required .'
    ],
    'email' => [
        'required' => 'Email is required .',
        'unique' => 'Email is taken .',
        'invalid' => 'Email address is invalid',
        'check' => 'email sent to reset your password, Please check your Email',
        'error' => 'Error!! , Please try again .',
        'no' => 'No account connect with this Email'
    ],
    'reason_for_contact' => [
        'required' => 'reason id required',
    ],
    'message' => [
        'required' => 'message is required',
    ],
    'password' => [
        'required' => 'Password is required .',
        'confirmed' => 'Password not confirmed .',
        'min' => 'Password must be minimum 6 characters .'
    ],
    'old_password' => [
        'required' => 'Old Password is required .',
        'confirmed' => 'Old Password not confirmed .'
    ],
    'social_token' => [
        'required' => 'Account ID is required .',
    ],
    'social_type' => [
        'required' => 'Account type is required .',
    ],
    'reservation_id' => [
        'required' => 'Reservation ID required .',
    ],
    'category' => [
        'required' => 'Category required .',
    ],
    'image' => [
        'required' => 'Image required .',
    ],
    'images' => [
        'required' => 'Images required .',
        'array' => 'Images should be array .',
    ],
    'img' => [
        'required' => 'Image required .',
    ],
    'price' => [
        'required' => 'Price required .',
    ],
    'amount' => [
        'required' => 'Amount required .',
    ],
    'price_for_edit' => [
        'required' => 'Price for edit required .',
    ],
    'basic_hours' => [
        'required' => 'Hours required .',
    ],
    'price_for_additional_hour' => [
        'required' => 'Price for additional hour required .',
    ],
    'service' => [
        'required' => 'Service ID required .',
        'success' => 'Saved successful',
        'error'    => 'Error!! , Please try again .',
    ],
    'device_type' => [
        'required' => 'Device type required .',
    ],
    'token' => [
        'required' => 'Token required .',
    ],
    'date' => [
        'required' => 'Date required .',
    ],
    'time' => [
        'required' => 'Time required .',
    ],
    'ampm' => [
        'required' => 'Am - PM  required .',
    ],
    'accept' => [
        'required' => ' required .',
    ],
    'portfolio' => [
        'required' => ' required .',
    ],
    'rate' => [
        'required' => 'Rate required .',
        'success' => 'Rate successful',
        'error'    => 'Error!! , Please try again .',
    ],
    'login'              => [
        'success' => 'login successful',
        'notCorrect' => 'username or password not correct',
        'error'    => 'Error!! , Please try again .',
        'banned'  => 'your account has been disabled please contact with administrator',
    ],
    'signup'              => [
        'success' => 'Sign up successful',
        'error'    => 'Error!! , Please try again .',
    ],
    'save_message'              => [
        'success' => 'Send message successful',
        'error'    => 'Error!! , Please try again .',
    ],
    'save'              => [
        'success' => 'Saved successful',
        'error'    => 'Error!! , Please try again .',
    ],
    'add'              => [
        'success' => 'Added successful',
        'error'    => 'Error!! , Please try again .',
    ],
    'delete'              => [
        'success' => 'Deleted successful',
        'error'    => 'Error!! , Please try again .',
    ],
    'update'              => [
        'success' => 'Update successful',
        'error'    => 'Error!! , Please try again .',
    ],
    'back'              => [
        'success' => 'loaded',
        'error'    => 'Error !',
    ],
    'confirm'              => [
        'success' => 'Confirmed',
        'error'    => 'Error!! , Please try again .',
    ],
];
