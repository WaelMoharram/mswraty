<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted'             => 'The must be accepted.',
    'name' => [
        'required' => 'الاسم مطلوب .',
        'unique' => 'الاسم مستخدم من قبل .'
    ],
    'mobile' => [
        'required' => 'رقم الجوال مطلوب .'
    ],
    'email' => [
        'required' => 'البريد الالكترونى مطلوب .',
        'unique' => 'البريد الالكترونى مستخدم من قبل .',
        'invalid' => 'البريد الالكتروني غير صالح',
        'check' => 'تم ارسال بريد الكترونى لاعادة كلمة المرور , برجاء فحص البريد الالكترونى',
        'error' => 'حدث خطأ ، حاول مرة أخرى',
        'no' => 'لا يوجد أي حساب مرتبط بهذا البريد الالكتروني'
    ],
    'reason_for_contact' => [
        'required' => 'يجب اختيار سبب التواصل',
    ],
    'message' => [
        'required' => 'الرسالة مطلوبة',
    ],
    'password' => [
        'required' => 'كلمة السر مطلوبة .',
        'confirmed' => 'كلمة المرور الجديدة غير مطابقة .',
        'min' => 'كلمة المرور يجب الا تقل عن 6 أحرف .'
    ],

    'old_password' => [
        'required' => 'كلمة السر القديمة  مطلوبة .',
        'confirmed' => 'كلمة المرور القديمة غير مطابقة .'
    ],
    'social_token' => [
        'required' => 'رقم الحساب مطلوب .',
    ],
    'social_type' => [
        'required' => 'نوع الحساب مطلوب .',
    ],
    'reservation_id' => [
        'required' => 'رقم الحجز مطلوب .',
    ],
    'category' => [
        'required' => 'الفئة مطلوبة .',
    ],
    'image' => [
        'required' => 'الصورة مطلوبة .',
    ],
    'images' => [
        'required' => 'الصور مطلوبة .',
        'array' => 'الصور يجب أن تكون مصفوفة صور .',
    ],
    'img' => [
        'required' => 'الصورة مطلوبة .',
    ],
    'price' => [
        'required' => 'السعر مطلوب .',
    ],
    'amount' => [
        'required' => 'الكمية مطلوبة .',
    ],
    'price_for_edit' => [
        'required' => 'السعر للتعديل مطلوب .',
    ],
    'basic_hours' => [
        'required' => 'عدد ساعات الخدمة مطلوب .',
    ],
    'price_for_additional_hour' => [
        'required' => 'السعر لكل ساعة اضافية مطلوب .',
    ],
    'service' => [
        'required' => 'رقم الخدمة مطلوب .',
        'success' => 'تم الحفظ بنجاح',
        'error'    => 'حدث خطأ من فضلك حاول مرة أخرى',
    ],
    'device_type' => [
        'required' => 'نوع الجهاز مطلوب .',
    ],
    'token' => [
        'required' => 'التوكن مطلوب .',
    ],
    'date' => [
        'required' => 'التاريخ  مطلوب .',
    ],
    'time' => [
        'required' => 'الوقت مطلوب .',
    ],
    'ampm' => [
        'required' => 'تحديد الوقت صباحا أو مساءا مطلوب .',
    ],
    'accept' => [
        'required' => ' مطلوب .',
    ],
    'portfolio' => [
        'required' => ' مطلوب .',
    ],
    'rate' => [
        'required' => 'التقييم مطلوب .',
        'success' => 'تم التقييم بنجاح',
        'error'    => 'حدث خطأ من فضلك حاول مرة أخرى',
    ],
    'login'              => [
        'success' => 'تم تسجيل الدخول بنجاح',
        'notCorrect' => 'بيانات الدخول غير صحيحة',
        'error'    => 'حدث خطأ من فضلك حاول مرة أخرى',
        'banned'  => 'لقد تم تعطيل حسابك , من فضلك تواصل مع الادارة',
    ],
    'signup'              => [
        'success' => 'تم التسجيل بنجاح',
        'error'    => 'حدث خطأ من فضلك حاول مرة أخرى',
    ],

    'save_message'              => [
        'success' => 'تم ارسال الرسالة بنجاح',
        'error'    => 'حدث خطأ من فضلك حاول مرة أخرى',
    ],
    'save'              => [
        'success' => 'تم الحفظ بنجاح',
        'error'    => 'حدث خطأ من فضلك حاول مرة أخرى',
    ],
    'add'              => [
        'success' => 'تم الاضافة بنجاح',
        'error'    => 'حدث خطأ من فضلك حاول مرة أخرى',
    ],
    'delete'              => [
        'success' => 'تم الحذف بنجاح',
        'error'    => 'حدث خطأ من فضلك حاول مرة أخرى',
    ],
    'update'              => [
        'success' => 'تم التحديث بنجاح',
        'error'    => 'حدث خطأ من فضلك حاول مرة أخرى',
    ],
    'back'              => [
        'success' => 'تم',
        'error'    => 'خطأ',
    ],
    'confirm'              => [
        'success' => 'تم التأكيد',
        'error'    => 'حدث خطأ من فضلك حاول مرة أخرى',
    ],



];
