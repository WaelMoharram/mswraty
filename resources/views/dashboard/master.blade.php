<!DOCTYPE html>
<html lang="en" dir="rtl">
    @include('dashboard.head')
    <body class="page-body">
        <div class="page-container horizontal-menu">
            @include('dashboard.header')
            <div class="main-content">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <h2>{{$page_title}}</h2>
                            <br />
                            @yield('content')
                            <div class="clearfix"></div>
                            <!-- Footer -->
                            <footer class="main">
                                &copy; 2017 <strong><a href="#">Mswraty</a></strong> .
                            </footer>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @include('dashboard.footer')
    </body>
</html>