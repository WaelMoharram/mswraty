<!-- Bottom scripts (common) -->
<script src="{{asset('neon/assets/js/gsap/TweenMax.min.js')}}"></script>
<script src="{{asset('neon/assets/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js')}}"></script>
<script src="{{asset('neon/assets/js/bootstrap.js')}}"></script>
<script src="{{asset('neon/assets/js/joinable.js')}}"></script>
<script src="{{asset('neon/assets/js/resizeable.js')}}"></script>
<script src="{{asset('neon/assets/js/neon-api.js')}}"></script>
<!-- JavaScripts initializations and stuff -->
<script src="{{asset('neon/assets/js/neon-custom.js')}}"></script>