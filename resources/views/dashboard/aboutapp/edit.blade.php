@extends('dashboard.master',[
                                'page_title'       => 'عن التطبيق'
                                ])

@section('content')
    <!-- form start -->
    {!! Form::model($model,[
                            'action'=>['AboutappController@update',$model->id],
                            'id'=>'myForm',
                            'role'=>'form',
                            'method'=>'PUT'
                            ])!!}
    @include('dashboard.aboutapp.form')
    <button type="submit" class="btn btn-primary">حفظ</button>
    {!! Form::close()!!}
@stop