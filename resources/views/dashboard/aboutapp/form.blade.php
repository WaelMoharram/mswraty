@include('dashboard.partials.validation-errors')
@include('flash::message')

<div class="form-group">
    <label for="">عن التطبيق "باللغة العربية"</label>
    {!! Form::textarea('about_ar', null ,[
        'class' => 'form-control',
        'placeholder' => 'عن التطبيق'
    ])!!}
</div>
<div class="form-group">
    <label for="">عن التطبيق "باللغة الانجليزية"</label>
    {!! Form::textarea('about_en', null ,[
        'class' => 'form-control',
        'placeholder' => 'about app'
    ])!!}
</div>
<div class="form-group">
    <label for="">الهاتف</label>
    {!! Form::text('phone', null ,[
        'class' => 'form-control',
        'placeholder' => 'الهاتف'
    ])!!}
</div>

<div class="form-group">
    <label for="">البريد الالكترونى</label>
    {!! Form::text('email', null ,[
        'class' => 'form-control',
        'placeholder' => 'البريد الالكترونى'
    ])!!}
</div>

<div class="form-group">
    <label for="">الموقع الالكترونى</label>
    {!! Form::text('website', null ,[
        'class' => 'form-control',
        'placeholder' => 'الموقع الاكترونى'
    ])!!}
</div>

<div class="form-group">
    <label for="">نسبة التطبيق من ارباح المصورين</label>
    {!! Form::number('percent', null ,[
        'class' => 'form-control',
        'min' => '0',
        'max' => '100',
        'step' => '1',
        'placeholder' => 'النسبة'
    ])!!}
</div>


