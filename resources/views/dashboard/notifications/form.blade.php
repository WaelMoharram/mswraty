@include('dashboard.partials.validation-errors')
@include('flash::message')

<div class="form-group">
    <label for="">ارسال لـ:</label>
    {!! Form::select('to',[0=>'الكل',1=>'المصورين',2=>'العملاء'],null ,[
        'class' => 'form-control'
    ])!!}
</div>

<div class="form-group">
    <label for="">الاشعار بالعربية</label>
    {!! Form::textarea('msg_ar', null ,[
        'class' => 'form-control',
        'placeholder' => 'الرسالة بالعربية'
    ])!!}
</div>

<div class="form-group">
    <label for="">الاشعار بالانجليزية</label>
    {!! Form::textarea('msg_en', null ,[
        'class' => 'form-control',
        'placeholder' => 'الرسالة بالانجليزية'
    ])!!}
</div>




