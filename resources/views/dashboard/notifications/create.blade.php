@extends('dashboard.master',[
                                'page_title'       => 'ارسال اشعارات'
                                ])

@section('content')

    <!-- form start -->
    {!! Form::open([
                            'action'=>'NotificationController@send',
                            'id'=>'myForm',
                            'role'=>'form',
                            'method'=>'POST'
                            ])!!}
        @include('dashboard.notifications.form')
        <button type="submit" class="btn btn-primary">ارسال</button>
    {!! Form::close()!!}
@stop