<header class="navbar navbar-fixed-top"><!-- set fixed position by adding class "navbar-fixed-top" -->

    <div class="navbar-inner">

        <!-- logo -->
        <div class="navbar-brand">
            <a href="{{url('admin')}}">
                <img src="{{asset('neon/assets/images/logo@2x.png')}}" width="88" alt="" />
            </a>
        </div>


        <!-- main menu -->

        <ul class="navbar-nav">

            <li class=" ">
                <a href="{{url('admin/customer')}}">
                    <i class="entypo-layout"></i>
                    <span class="title">العملاء</span>
                </a>
            </li>

            <li class=" ">
                <a href="{{url('admin/photographer')}}">
                    <i class="entypo-layout"></i>
                    <span class="title">المصورون</span>
                </a>
            </li>

            <li class=" ">
                <a href="{{url('admin/service')}}">
                    <i class="entypo-layout"></i>
                    <span class="title">الخدمات</span>
                </a>
            </li>

            <li class=" ">
                <a href="{{url('admin/reservation')}}">
                    <i class="entypo-layout"></i>
                    <span class="title">الحجوزات</span>
                </a>
            </li>

            <li class="has-sub">
                <a>
                    <i class="entypo-layout"></i>
                    <span class="title">رسائل التواصل</span>
                </a>
                <ul class="visible">
                    <li>
                        <a href="{{url('admin/message')}}">
                            <span class="title">الرسائل</span>
                        </a>
                        <a href="{{url('admin/reason')}}">
                            <span class="title">أسباب التواصل</span>
                        </a>
                    </li>

                </ul>
            </li>

            <li class=" ">
                <a href="{{url('admin/notification')}}">
                    <i class="entypo-layout"></i>
                    <span class="title">ارسال اشعارات</span>
                </a>
            </li>

            <li class=" ">
                <a href="{{url('admin/profit_report')}}">
                    <i class="entypo-layout"></i>
                    <span class="title">تقرير الأرباح</span>
                </a>
            </li>

            <li class=" has-sub">
                <a>
                    <i class="entypo-layout"></i>
                    <span class="title">الاعدادات</span>
                </a>
                <ul class="visible">
                    <li>
                        <a href="{{url('admin/admin')}}">
                            <span class="title">مديرين التطبيق</span>
                        </a>
                        <a href="{{url('admin/city')}}">
                            <span class="title">المدن</span>
                        </a>
                        <a href="{{url('admin/category')}}">
                            <span class="title">الأقسام</span>
                        </a>
                        <a href="{{url('admin/about_app/1/edit')}}">
                            <span class="title">عن التطبيق</span>
                        </a>
                    </li>

                </ul>
            </li>

        </ul>


        <!-- notifications and other links -->
        <ul class="nav navbar-right pull-right">





            <li class="sep"></li>

            <li>

                <a href="{{ url('/logout') }}"
                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                    تسجيل الخروج <i class="entypo-logout right"></i>
                </a>

                <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>

            </li>


            <!-- mobile only -->
            <li class="visible-xs">

                <!-- open/close menu icon (do not remove if you want to enable menu on mobile devices) -->
                <div class="horizontal-mobile-menu visible-xs">
                    <a href="#" class="with-animation"><!-- add class "with-animation" to support animation -->
                        <i class="entypo-menu"></i>
                    </a>
                </div>

            </li>

        </ul>

    </div>

</header>