@extends('dashboard.master',[
                                'page_title'       => 'تقرير عمليات المصور'
                                ])
@section('content')
    @include('flash::message')
    @if(count($transactions))
<h2>{{$photographer->name}}</h2>
        <div class="table-responsive">
            <table class="table table-bordered">
                <thead>
                <th>#</th>
                <th>التاريخ</th>
                <th>المبلغ</th>
                <th>المستحق للمصور</th>
                <th>المستحق للتطبيق</th>
                </thead>
                <tbody>
                @php $count = 1; @endphp
                @foreach($transactions as $transaction)

                    <tr id="removable{{$transaction->id}}">
                        <td>{{$count}}</td>
                        <td>{{$transaction->created_at}}</td>
                        <td>{{$transaction->amount}}</td>
                        <td>{{$transaction->photographer_amount}}</td>
                        <td>{{$transaction->app_amount}}</td>
                    </tr>
                    @php $count ++; @endphp
                @endforeach
                </tbody>
            </table>
        </div>
        {!! $transactions->render() !!}
    @endif
@endsection