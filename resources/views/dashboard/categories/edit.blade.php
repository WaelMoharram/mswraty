@extends('dashboard.master',[
                                'page_title'       => 'الأقسام'
                                ])

@section('content')
    <!-- form start -->
    {!! Form::model($model,[
                            'action'=>['CategoryController@update',$model->id],
                            'id'=>'myForm',
                            'role'=>'form',
                            'method'=>'PUT',
                            'enctype'=>'multipart/form-data'
                            ])!!}
    @include('dashboard.categories.form')
    <button type="submit" class="btn btn-primary">حفظ</button>
    {!! Form::close()!!}
@stop