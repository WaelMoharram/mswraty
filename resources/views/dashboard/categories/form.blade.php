@include('dashboard.partials.validation-errors')
@include('flash::message')

<div class="form-group">
    <label for="">القسم بالعربية</label>
    {!! Form::text('name_ar', null ,[
        'class' => 'form-control',
        'placeholder' => 'القسم'
    ])!!}
</div>

<div class="form-group">
    <label for="">القسم بالانجليزية</label>
    {!! Form::text('name_en', null ,[
        'class' => 'form-control',
        'placeholder' => 'category'
    ])!!}
</div>

<div class="form-group">
    <label for="">ايفونة القسم</label>
    {!! Form::file('icon', null ,[
        'class' => 'form-control',
        'placeholder' => 'ايقونة'
    ])!!}
</div>




