@extends('dashboard.master',[
                                'page_title'       => 'أسباب التواصل'
                                ])
@section('content')
    @include('flash::message')
    @if(count($categories))
        <div class="col-md-12">
            <a href="{{url('admin/category/create')}}" class="btn btn-info">اضافة قسم جديد</a>
        </div>
        <br>
        <br>
        <div class="clearfix"></div>
        <div class="table-responsive">
            <table class="table table-bordered">
                <thead>
                <th>#</th>
                <th>الاسم بالعربية</th>
                <th>الاسم بالانجليزية</th>
                <th>ايقونة القسم</th>
                <th>الحالة</th>
                <th class="text-center">تعديل</th>
                <th class="text-center">حذف</th>
                </thead>
                <tbody>
                @php $count = 1; @endphp
                @foreach($categories as $category)
                    <tr id="removable{{$category->id}}">
                        <td>{{$count}}</td>
                        <td>{{$category->name_ar}}</td>
                        <td>{{$category->name_en}}</td>
                        <td><img  style="max-height:60px;" src="../../{{$category->icon}}"></td>
                        <td class="text-center">
                            @if($category->enabled)
                                <a href="category/{{$category->id}}/disabled" class="btn btn-xs btn-success"><i class="fa fa-check"></i> مفعل</a>
                            @else
                                <a href="category/{{$category->id}}/enabled" class="btn btn-xs btn-danger"><i class="fa fa-close"></i> غير مفعل</a>
                            @endif
                        </td>
                        <td class="text-center"><a href="category/{{$category->id}}/edit" class="btn btn-xs btn-success"><i class="fa fa-edit"></i></a></td>
                        <td class="center">
                            <a class="btn btn-xs btn-danger" data-toggle="modal" data-target="#delete{{$category->id}}"><i class="entypo-trash"></i></a>
                        </td>
                        {{--<td class="text-center">--}}
                        {{--<button id="{{$category->id}}" data-token="{{ csrf_token() }}" data-route="{{URL::route('category.destroy',$category->id)}}"  type="button" class="destroy btn btn-danger btn-xs"><i class="fa fa-trash-o"></i></button>--}}
                        {{--</td>--}}






                    </tr>
                    <div id="delete{{$category->id}}" class="modal fade" role="dialog">
                        <div class="modal-dialog">
                            <!-- Modal content-->
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title">حذف القسم</h4>
                                </div>
                                <div class="modal-body">
                                    <p>هل أنت متأكد من حذف القسم {{$category->name_ar}} ؟</p>
                                </div>
                                <div class="modal-footer">
                                    {!! Form::model($category,[
                                'action'=>['CategoryController@destroy',$category->id],
                                'id'=>'myForm',
                                'role'=>'form',
                                'method'=>'delete'
                                ])!!}
                                    <button type="submit" class="btn btn-danger">حذف</button>
                                    {!! Form::close()!!}
                                    <button type="button" class="btn btn-default" data-dismiss="modal">لا</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    @php $count ++; @endphp
                @endforeach
                </tbody>
            </table>
        </div>
        {!! $categories->render() !!}
    @endif
@endsection