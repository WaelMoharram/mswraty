@extends('dashboard.master',[
                                'page_title'       => 'الحجوزات'
                                ])
@section('content')
    @include('flash::message')
    @if(count($reservations))

        <div class="clearfix"></div>
        <div class="table-responsive">
            <table class="table table-bordered">
                <thead>
                <th>#</th>
                <th>العميل</th>
                <th>المصور</th>
                <th>الخدمة</th>
                <th>تاريخ الخدمة</th>
                <th>توقيت الخدمة</th>
                <th>صباحا/مساءا</th>
                <th>الحالة</th>
                <th>التقييم</th>
                <th>السعر</th>
                <th>الكمية</th>
                <th>السعر لكل تعديل</th>
                <th>الكمية المعدلة</th>
                <th>عدد ساعات الخدمة</th>
                <th>تكلفة الساعة الاضافية</th>
                <th>طريقة الدفع</th>
                </thead>
                <tbody>
                @php $count = 1; @endphp
                @foreach($reservations as $reservation)
                    @php if($reservation->status=='done'){
                    $reservation->status='منتهى';
                    $btn_color='btn-success';
                    }elseif($reservation->status=='waitingapproval') {
                    $reservation->status = 'فى انتظار الموافقة';
                    $btn_color='btn-warning';
                    }else {
                    $reservation->status = 'فى انتظار التنفيذ';
                    $btn_color='btn-info';
                    }

                    if($reservation->pay_method=='cash'){
                    $reservation->pay_method='نقدى';
                    }
                    if(date("a", strtotime($reservation->created_at))=='am'){
                    $ampm= 'صباحا';
                    }else{
                    $ampm= 'مساءا';
                    }

                    if(optional($reservation->photographer)->gender=='male'){
                        $pg='ذكر';
                    }else {
                        $pg='انثى';
                    }

                    if(optional($reservation->customer)->gender=='male'){
                        $cg='ذكر';
                    }else {
                        $cg='انثى';
                    }

                    $pimage=optional($reservation->photographer)->image;
                    $pcimage=optional($reservation->photographer)->cover_image;
                    $cimage=optional($reservation->customer)->image;
                       if($pimage == ""){
                            $pimage = "uploads/image_placeholder.png";
                       }
                       if($pcimage == ""){
                            $pcimage = "uploads/cover_placeholder.png";
                       }
                       if($cimage == ""){
                            $cimage = "uploads/image_placeholder.png";
                       }
                    @endphp
                    <tr id="removable{{$reservation->id}}">
                        <td>{{$count}}</td>
                        <td><a class="btn btn-info btn-xs" data-toggle="modal" data-target="#customer{{$reservation->id}}">{{optional($reservation->customer)->name}}</a></td>
                        <td><a class="btn btn-success btn-xs" data-toggle="modal" data-target="#photographer{{$reservation->id}}">{{optional($reservation->photographer)->name}}</a></td>
                        <td>{{ optional($reservation->service)->name }}</td>
                        <td>{{date("d-m-Y", strtotime($reservation->created_at))}}</td>
                        <td>{{date("h:i:s", strtotime($reservation->created_at))}}</td>
                        <td>{{$ampm}}</td>
                        <td class="text-center"><button class="btn btn-xs {{$btn_color}}">{{$reservation->status}}</button></td>
                        <td>{{$reservation->rate}}</td>
                        <td>{{$reservation->price}}</td>
                        <td>{{$reservation->amount}}</td>
                        <td>{{$reservation->price_for_edit}}</td>
                        <td>{{$reservation->edit_amount}}</td>
                        <td>{{$reservation->basic_hours}}</td>
                        <td>{{$reservation->price_for_additional_hour}}</td>
                        <td class="text-center"><button class="btn btn-xs btn-success">{{$reservation->pay_method}}</button></td>






                    </tr>


                    <div id="photographer{{$reservation->id}}" id="model-3" class="modal fade custom-width" role="dialog">
                        <div class="modal-dialog" style="width: 96%">
                            <!-- Modal content-->
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title">{{optional($reservation->photographer)->name}}</h4>
                                </div>
                                <div class="modal-body">



                                    <div class="profile-env">
                                        <header class="row">
                                            <div class="col-md-12" style="height: 400px; overflow: hidden;">
                                                <img style="margin: 0 auto;" src="../../{{$pcimage}}" class="img-responsive"/>
                                            </div>
                                            <div class="col-sm-2">

                                                <a href="#" class="profile-picture">

                                                    <img src="../../{{$pimage}}" class="img-responsive img-circle" style="height: 150px;"/>
                                                </a>

                                            </div>

                                            <div class="col-sm-7">

                                                <ul class="profile-info-sections">
                                                    <li>
                                                        <div class="profile-name">
                                                            <strong>
                                                                <a href="#">{{optional($reservation->photographer)->name}}</a>
                                                                <!-- User statuses available classes "is-online", "is-offline", "is-idle", "is-busy" -->						</strong>
                                                            <span>مصور</span>
                                                            <span>{{$pg}}</span>
                                                        </div>
                                                    </li>

                                                    <li>
                                                        <div class="profile-stat">
                                                            <h3>{{optional($reservation->photographer)->rate}}</h3>
                                                            <span><a href="#">التقييم</a></span>
                                                        </div>
                                                    </li>

                                                    <li>
                                                        <div class="profile-stat">
                                                            <h3>{{optional($reservation->photographer)->rate_count}}</h3>
                                                            <span><a href="#">عدد التقييمات </a></span>
                                                        </div>
                                                    </li>

                                                    <li>
                                                        <div class="profile-stat">
                                                            <h3>{{optional($reservation->photographer)->min_price}}</h3>
                                                            <span><a href="#">أقل سعر لخدمة  </a></span>
                                                        </div>
                                                    </li>
                                                </ul>

                                            </div>

                                            <div class="col-sm-3">

                                            </div>

                                        </header>

                                        <section class="profile-info-tabs" style="margin-left: 0px; margin-right: 0px; ">

                                            <div class="row">

                                                <div class="col-sm-offset-2 col-sm-10">

                                                    <ul class="user-details">
                                                        <li>
                                                            <a href="#">
                                                                <i class="entypo-email"></i>
                                                                {{optional($reservation->photographer)->email}}
                                                            </a>
                                                        </li>

                                                        <li>
                                                            <a href="#">
                                                                <i class="entypo-mobile"></i>
                                                                {{optional($reservation->photographer)->mobile}}
                                                            </a>
                                                        </li>

                                                        <li>
                                                            <a href="#">
                                                                <i class="entypo-location"></i>
                                                                {{ optional(optional($reservation->photographer)->city)->name_ar }}
                                                            </a>
                                                        </li>

                                                        <li>
                                                            <a href="#">
                                                                <i class="entypo-suitcase"></i>
                                                                {{optional($reservation->photographer)->description}}
                                                            </a>
                                                        </li>

                                                        <li>
                                                            <a href="#">
                                                                <i class="entypo-chart-area"></i>
                                                                {{optional($reservation->photographer)->from_year}}
                                                            </a>
                                                        </li>

                                                        <li>
                                                            <a href="#">
                                                                <i class="entypo-docs"></i>
                                                                {{optional($reservation->photographer)->categories_human}}
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>

                                            </div>

                                        </section>

                                    </div>


                                </div>

                            </div>
                        </div>
                    </div>


                    <div id="customer{{$reservation->id}}" id="model-3" class="modal fade custom-width" role="dialog">
                        <div class="modal-dialog" style="width: 96%">
                            <!-- Modal content-->
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title">{{optional($reservation->customer)->name}}</h4>
                                </div>
                                <div class="modal-body">



                                    <div class="profile-env">
                                        <header class="row">
                                            <div class="col-sm-2">

                                                <a href="#" class="profile-picture">

                                                    <img src="../../{{$cimage}}" class="img-responsive img-circle" style="height: 150px;"/>
                                                </a>

                                            </div>

                                            <div class="col-sm-7">

                                                <ul class="profile-info-sections">
                                                    <li>
                                                        <div class="profile-name">
                                                            <strong>
                                                                <a href="#">{{optional($reservation->customer)->name}}</a>
                                                                <!-- User statuses available classes "is-online", "is-offline", "is-idle", "is-busy" -->						</strong>
                                                            <span>عميل</span>
                                                            <span>{{$cg}}</span>
                                                        </div>
                                                    </li>


                                                </ul>

                                            </div>

                                            <div class="col-sm-3">

                                            </div>

                                        </header>

                                        <section class="profile-info-tabs" style="margin-left: 0px; margin-right: 0px; ">

                                            <div class="row">

                                                <div class="col-sm-offset-2 col-sm-10">

                                                    <ul class="user-details">
                                                        <li>
                                                            <a href="#">
                                                                <i class="entypo-mail"></i>
                                                                {{optional($reservation->customer)->email}}
                                                            </a>
                                                        </li>

                                                        <li>
                                                            <a href="#">
                                                                <i class="entypo-mobile"></i>
                                                                {{optional($reservation->customer)->mobile}}
                                                            </a>
                                                        </li>

                                                        <li>
                                                            <a href="#">
                                                                <i class="entypo-location"></i>
                                                                {{ optional(optional($reservation->customer)->city)->name_ar }}
                                                            </a>
                                                        </li>

                                                    </ul>
                                                </div>

                                            </div>

                                        </section>

                                    </div>


                                </div>

                            </div>
                        </div>
                    </div>

                    @php $count ++; @endphp
                @endforeach
                </tbody>
            </table>
        </div>
        {!! $reservations->render() !!}
    @endif
@endsection