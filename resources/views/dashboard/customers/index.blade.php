@extends('dashboard.master',[
                                'page_title'       => 'العملاء'
                                ])
@inject('cities','App\Models\City')
<?php
$cities =  [0=>'الكل']+$cities->pluck('name_ar', 'id')->toArray();
?>
@section('content')
    @include('flash::message')
    @if(count($customers))

        <div>
            {!! Form::open([
                            'id'=>'myForm',
                            'role'=>'form',
                            'method'=>'Get'
                            ])!!}
            <div class="form-group col-md-4">
                <label for="">الاسم</label>
                {!! Form::text('name', null ,[
                    'class' => 'form-control',
                    'placeholder' => 'الاسم'
                ])!!}
            </div>

            <div class="form-group col-md-4">
                <label for="">النوع:</label>
                {!! Form::select('gender',[0=>'الكل','male'=>'ذكر','female'=>'انثى'],null ,[
                    'class' => 'form-control'
                ])!!}
            </div>
            <div class="form-group col-md-4">
                <label for="">المدينة:</label>
                {!! Form::select('city_id',$cities,null ,[
                    'class' => 'form-control'
                ])!!}
            </div>
            <div class="clearfix"></div>
            <button type="submit" class="btn btn-primary">تصفية</button>
            {!! Form::close()!!}
        </div>
        <br>
        <div class="clearfix"></div>

        <div class="table-responsive">
            <table class="table table-bordered">
                <thead>
                <th>#</th>
                <th>الصورة</th>
                <th>الاسم</th>
                <th>البريد الالكترونى</th>
                <th>الهاتف</th>
                <th>المدينة</th>
                <th>النوع</th>
                <th class="text-center">الحالة</th>
                </thead>
                <tbody>
                @php $count = 1; @endphp
                @foreach($customers as $customer)
                    @php
                        $btn_color = null;
                            if($customer->gender=='male'){
                        $customer->gender='ذكر';
                        $btn_color='btn-info';
                        }elseif($customer->gender=='female') {
                        $customer->gender = 'انثى';
                        $btn_color='btn-orange';
                        }
                        if($customer->image == '' || $customer->image == Null){
                            $customer->image = "uploads/image_placeholder.png";
                       }
                       if($customer->cover_image == '' || $customer->cover_image == Null){
                            $customer->cover_image = "uploads/cover_placeholder.png";
                       }
                    @endphp
                    <tr id="removable{{$customer->id}}">
                        <td>{{$count}}</td>
                        <td class="text-center"><img src="../../{{$customer->image}}" class="img-responsive" style="height: 50px; margin: 0 auto;"/></td>
                        <td><a class="btn btn-success btn-xs" data-toggle="modal" data-target="#show{{$customer->id}}">{{$customer->name}}</a></td>
                        <td>{{$customer->email}}</td>
                        <td>{{$customer->mobile}}</td>
                        <td>{{ optional($customer->city)->name_ar }}</td>
                        @if($btn_color != null)
                            <td class="text-center"><button class="btn btn-xs disabled {{$btn_color}}">{{$customer->gender}}</button></td>
                        @else
                            <td></td>
                        @endif
                        <td class="text-center">
                            @if($customer->enabled)
                                <a href="customer/{{$customer->id}}/disabled" class="btn btn-xs btn-success"><i class="fa fa-check"></i> مفعل</a>
                            @else
                                <a href="customer/{{$customer->id}}/enabled" class="btn btn-xs btn-danger"><i class="fa fa-close"></i> غير مفعل</a>
                            @endif
                        </td>
                    </tr>
                    <div id="show{{$customer->id}}" id="model-3" class="modal fade custom-width" role="dialog">
                        <div class="modal-dialog" style="width: 96%">
                            <!-- Modal content-->
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title">{{$customer->name}}</h4>
                                </div>
                                <div class="modal-body">



                                    <div class="profile-env">
                                        <header class="row">
                                            <div class="col-sm-2">

                                                <a href="#" class="profile-picture">

                                                    <img src="../../{{$customer->image}}" class="img-responsive img-circle" style="height: 150px;"/>
                                                </a>

                                            </div>

                                            <div class="col-sm-7">

                                                <ul class="profile-info-sections">
                                                    <li>
                                                        <div class="profile-name">
                                                            <strong>
                                                                <a href="#">{{$customer->name}}</a>
                                                                <!-- User statuses available classes "is-online", "is-offline", "is-idle", "is-busy" -->						</strong>
                                                            <span>عميل</span>
                                                            <span >{{$customer->gender}}</span>
                                                        </div>
                                                    </li>


                                                </ul>

                                            </div>

                                            <div class="col-sm-3">

                                            </div>

                                        </header>

                                        <section class="profile-info-tabs" style="margin-left: 0px; margin-right: 0px; ">

                                            <div class="row">

                                                <div class="col-sm-offset-2 col-sm-10">

                                                    <ul class="user-details">
                                                        <li>
                                                            <a href="#">
                                                                <i class="entypo-mail"></i>
                                                                {{$customer->email}}
                                                            </a>
                                                        </li>

                                                        <li>
                                                            <a href="#">
                                                                <i class="entypo-mobile"></i>
                                                                {{$customer->mobile}}
                                                            </a>
                                                        </li>

                                                        <li>
                                                            <a href="#">
                                                                <i class="entypo-location"></i>
                                                                {{ optional($customer->city)->name_ar }}
                                                            </a>
                                                        </li>

                                                    </ul>
                                                </div>

                                            </div>

                                        </section>

                                    </div>


                                </div>

                            </div>
                        </div>
                    </div>
                    @php $count ++; @endphp
                @endforeach
                </tbody>
            </table>
        </div>
        {!! $customers->render() !!}
    @endif
@endsection