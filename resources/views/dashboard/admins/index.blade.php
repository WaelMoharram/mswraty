@extends('dashboard.master',[
                                'page_title'       => 'المديرين'
                                ])
@section('content')
@include('flash::message')
    @if(count($admins))
        <div class="col-md-12">
            <a href="{{url('admin/admin/create')}}" class="btn btn-info">اضافة مدير جديد</a>
        </div>
        <br>
        <br>
<div class="clearfix"></div>
        <div class="table-responsive">
            <table class="table table-bordered">
                <thead>
                <th>#</th>
                <th>اسم </th>
                <th>الايميل</th>
                <th class="text-center">تعديل</th>
                <th class="text-center">حذف</th>
                </thead>
                <tbody>
                @php $count = 1; @endphp
                @foreach($admins as $admin)
                    <tr id="removable{{$admin->id}}">
                        <td>{{$count}}</td>
                        <td>{{$admin->name}}</td>
                        <td>{{$admin->email}}</td>
                        <td class="text-center"><a href="admin/{{$admin->id}}/edit" class="btn btn-xs btn-success"><i class="fa fa-edit"></i></a></td>
                        <td class="center">
                            <a class="btn btn-xs btn-danger" data-toggle="modal" data-target="#delete{{$admin->id}}"><i class="entypo-trash"></i></a>
                        </td>
                        {{--<td class="text-center">--}}
                            {{--<button id="{{$admin->id}}" data-token="{{ csrf_token() }}" data-route="{{URL::route('admin.destroy',$admin->id)}}"  type="button" class="destroy btn btn-danger btn-xs"><i class="fa fa-trash-o"></i></button>--}}
                        {{--</td>--}}






                    </tr>
                    <div id="delete{{$admin->id}}" class="modal fade" role="dialog">
                    <div class="modal-dialog">
                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">حذف المدير</h4>
                            </div>
                            <div class="modal-body">
                                <p>هل أنت متأكد من حذف المدير {{$admin->name}} ؟</p>
                            </div>
                            <div class="modal-footer">
                                {!! Form::model($admin,[
                            'action'=>['UserController@destroy',$admin->id],
                            'id'=>'myForm',
                            'role'=>'form',
                            'method'=>'delete'
                            ])!!}
                                <button type="submit" class="btn btn-danger">حذف</button>
                                {!! Form::close()!!}
                                <button type="button" class="btn btn-default" data-dismiss="modal">لا</button>
                            </div>
                        </div>
                    </div>
        </div>
                    @php $count ++; @endphp
                @endforeach
                </tbody>
            </table>
        </div>
        {!! $admins->render() !!}
    @endif
@endsection