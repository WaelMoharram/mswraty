@include('dashboard.partials.validation-errors')
@include('flash::message')

<div class="form-group">
    <label for="">الاسم</label>
    {!! Form::text('name', null ,[
        'class' => 'form-control',
        'placeholder' => 'الاسم'
    ])!!}
</div>
<div class="form-group">
    <label for="">البريد الالكترونى</label>
    {!! Form::text('email', null ,[
        'class' => 'form-control',
        'placeholder' => 'البريد الالكترونى'
    ])!!}
</div>
<div class="form-group">
    <label for="">كلمة المرور</label>
    {!! Form::password('password',[
        'class' => 'form-control',
        'placeholder' => 'كلمة المرور'
    ])!!}
</div>
<div class="form-group">
    <label for="">اعادة كلمة المرور</label>
    {!! Form::password('password_confirmation' ,[
        'class' => 'form-control',
        'placeholder' => 'اعادة كلمة المرور'
    ])!!}
</div>



