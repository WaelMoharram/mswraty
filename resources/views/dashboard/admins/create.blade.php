@extends('dashboard.master',[
                                'page_title'       => 'المديرين'
                                ])

@section('content')

    <!-- form start -->
    {!! Form::model($model,[
                            'action'=>'UserController@store',
                            'id'=>'myForm',
                            'role'=>'form',
                            'method'=>'POST'
                            ])!!}
        @include('dashboard.admins.form')
        <button type="submit" class="btn btn-primary">حفظ</button>
    {!! Form::close()!!}
@stop