@extends('dashboard.master',[
                                'page_title'       => 'الخدمات'
                                ])
@inject('categories','App\Models\Category')
@inject('photographers','App\Models\Photographer')
<?php
$categories =  [0=>'الكل']+$categories->pluck('name_ar', 'id')->toArray();
$photographers =  [0=>'الكل']+$photographers->pluck('name', 'id')->toArray();
?>
@section('content')
    @include('flash::message')
    @if(count($services))

        <div>
            {!! Form::open([
                            'id'=>'myForm',
                            'role'=>'form',
                            'method'=>'Get'
                            ])!!}

            <div class="form-group col-md-4">
                <label for="">المصور:</label>
                {!! Form::select('photographer_id',$photographers,null ,[
                    'class' => 'form-control'
                ])!!}
            </div>

            <div class="form-group col-md-4">
                <label for="">اسم الخدمة</label>
                {!! Form::text('name', null ,[
                    'class' => 'form-control',
                    'placeholder' => 'الخدمة'
                ])!!}
            </div>


            <div class="form-group col-md-4">
                <label for="">القسم:</label>
                {!! Form::select('category_id',$categories,null ,[
                    'class' => 'form-control'
                ])!!}
            </div>

            <div class="form-group col-md-6">
                <label for="">السعر من</label>
                {!! Form::text('from', null ,[
                    'class' => 'form-control',
                    'placeholder' => 'من'
                ])!!}
            </div>

            <div class="form-group col-md-6">
                <label for="">السعر الى</label>
                {!! Form::text('to', null ,[
                    'class' => 'form-control',
                    'placeholder' => 'الى'
                ])!!}
            </div>

            <div class="clearfix"></div>
            <button type="submit" class="btn btn-primary">تصفية</button>
            {!! Form::close()!!}
        </div>
        <br>
        <div class="clearfix"></div>


        <div class="clearfix"></div>
        <div class="table-responsive">
            <table class="table table-bordered">
                <thead>
                <th>#</th>
                <th>المصور</th>
                <th>القسم</th>
                <th>اسم الخدمة</th>
                <th>السعر</th>
                <th>الكمية</th>
                <th>السعر لكل تعديل</th>
                <th>عدد ساعات الخدمة</th>
                <th>تكلفة الساعة الاضافية</th>
                <th>الحالة</th>
                </thead>
                <tbody>
                @php $count = 1; @endphp
                @foreach($services as $service)
                    @php
                        if(optional($service->photographer)->gender=='male'){
                            $pg='ذكر';
                            $btn_color='btn-info';
                        }else {
                            $pg='انثى';
                            $btn_color='btn-orange';
                        }

                        $pimage=optional($service->photographer)->image;
                        $pcimage=optional($service->photographer)->cover_image;
                            if($pimage == ""){
                                $pimage = "uploads/image_placeholder.png";
                           }
                           if($pcimage == ""){
                                $pcimage = "uploads/cover_placeholder.png";
                           }
                    @endphp
                    <tr id="removable{{$service->id}}">
                        <td>{{$count}}</td>
                        <td><a class="btn btn-success btn-xs" data-toggle="modal" data-target="#show{{$service->id}}">{{optional($service->photographer)->name}}</a></td>
                        <td>{{ optional($service->category)->name_ar }}</td>
                        <td>{{$service->name}}</td>
                        <td>{{$service->price}}</td>
                        <td>{{$service->amount}}</td>
                        <td>{{$service->price_for_edit}}</td>
                        <td>{{$service->basic_hours}}</td>
                        <td>{{$service->price_for_additional_hour}}</td>
                        <td class="text-center">
                            @if($service->enabled)
                                <a href="service/{{$service->id}}/disabled" class="btn btn-xs btn-success"><i class="fa fa-check"></i> مفعل</a>
                            @else
                                <a href="service/{{$service->id}}/enabled" class="btn btn-xs btn-danger"><i class="fa fa-close"></i> غير مفعل</a>
                            @endif
                        </td>





                    </tr>
                    <div id="show{{$service->id}}" id="model-3" class="modal fade custom-width" role="dialog">
                        <div class="modal-dialog" style="width: 96%">
                            <!-- Modal content-->
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title">{{optional($service->photographer)->name}}</h4>
                                </div>
                                <div class="modal-body">



                                    <div class="profile-env">
                                        <header class="row">
                                            <div class="col-md-12" style="height: 400px; overflow: hidden;">
                                                <img style="margin: 0 auto;" src="../../{{$pcimage}}" class="img-responsive"/>
                                            </div>
                                            <div class="col-sm-2">

                                                <a href="#" class="profile-picture">

                                                    <img src="../../{{$pimage}}" class="img-responsive img-circle" style="height: 150px;"/>
                                                </a>

                                            </div>

                                            <div class="col-sm-7">

                                                <ul class="profile-info-sections">
                                                    <li>
                                                        <div class="profile-name">
                                                            <strong>
                                                                <a href="#">{{optional($service->photographer)->name}}</a>
                                                                <!-- User statuses available classes "is-online", "is-offline", "is-idle", "is-busy" -->						</strong>
                                                            <span>مصور</span>
                                                            <span>{{$pg}}</span>
                                                        </div>
                                                    </li>

                                                    <li>
                                                        <div class="profile-stat">
                                                            <h3>{{optional($service->photographer)->rate}}</h3>
                                                            <span><a href="#">التقييم</a></span>
                                                        </div>
                                                    </li>

                                                    <li>
                                                        <div class="profile-stat">
                                                            <h3>{{optional($service->photographer)->rate_count}}</h3>
                                                            <span><a href="#">عدد التقييمات </a></span>
                                                        </div>
                                                    </li>

                                                    <li>
                                                        <div class="profile-stat">
                                                            <h3>{{optional($service->photographer)->min_price}}</h3>
                                                            <span><a href="#">أقل سعر لخدمة  </a></span>
                                                        </div>
                                                    </li>
                                                </ul>

                                            </div>

                                            <div class="col-sm-3">

                                            </div>

                                        </header>

                                        <section class="profile-info-tabs" style="margin-left: 0px; margin-right: 0px; ">

                                            <div class="row">

                                                <div class="col-sm-offset-2 col-sm-10">

                                                    <ul class="user-details">
                                                        <li>
                                                            <a href="#">
                                                                <i class="entypo-email"></i>
                                                                {{optional($service->photographer)->email}}
                                                            </a>
                                                        </li>

                                                        <li>
                                                            <a href="#">
                                                                <i class="entypo-mobile"></i>
                                                                {{optional($service->photographer)->mobile}}
                                                            </a>
                                                        </li>

                                                        <li>
                                                            <a href="#">
                                                                <i class="entypo-location"></i>
                                                                {{ optional(optional($service->photographer)->city)->name_ar }}
                                                            </a>
                                                        </li>

                                                        <li>
                                                            <a href="#">
                                                                <i class="entypo-suitcase"></i>
                                                                {{optional($service->photographer)->description}}
                                                            </a>
                                                        </li>

                                                        <li>
                                                            <a href="#">
                                                                <i class="entypo-chart-area"></i>
                                                                {{optional($service->photographer)->from_year}}
                                                            </a>
                                                        </li>

                                                        <li>
                                                            <a href="#">
                                                                <i class="entypo-docs"></i>
                                                                {{optional($service->photographer)->categories_human}}
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>

                                            </div>

                                        </section>

                                    </div>


                                </div>

                            </div>
                        </div>
                    </div>
                    @php $count ++; @endphp
                @endforeach
                </tbody>
            </table>
        </div>
        {!! $services->render() !!}
    @endif
@endsection