@extends('dashboard.master',[
                                'page_title'       => 'رئيسية لوحة التحكم '
                                ])
@section('content')
    @if(count($transactions))

        <h2>ارصدة العمليات</h2>

<div class="row">
    <div class="col-sm-12">
        <a href="{{url('admin/profit_report')}}">
            <div class="tile-stats tile-aqua">
                <div class="icon"><i class="entypo-suitcase"></i></div>
                <div class="num" data-start="0" data-end="{{$transactions->sum('amount')}}"  data-postfix=" ريال" data-duration="1500" data-delay="0"></div>
                <h3>تقرير الأرباح للمصورين</h3>
                <p>تقرير مفصل للارباح و العمليات</p>
            </div>
        </a>
    </div>
    <div class="col-sm-4">

        <div class="tile-stats tile-aqua">
            <div class="icon"><i class="entypo-suitcase"></i></div>
            <div class="num" data-start="0" data-end="{{$transactions->sum('amount')}}"  data-postfix=" ريال" data-duration="1500" data-delay="0"></div>
            <h3>اجمالى العمليات</h3>
            <p>اجمالى مبالغ العمليات</p>
        </div>

    </div>

    <div class="col-sm-4">

        <div class="tile-stats tile-red">
            <div class="icon"><i class="entypo-suitcase"></i></div>
            <div class="num" data-start="0" data-end="{{$transactions->sum('photographer_amount')}}"  data-postfix=" ريال" data-duration="1500" data-delay="0"></div>
            <h3>اجمالى المصورين</h3>
            <p>اجمالى مبالغ المصورين</p>
        </div>

    </div>

    <div class="col-sm-4">

        <div class="tile-stats tile-green">
            <div class="icon"><i class="entypo-suitcase"></i></div>
            <div class="num" data-start="-1" data-end="{{$transactions->sum('app_amount')}}"  data-postfix=" ريال" data-duration="1500" data-delay="0"></div>
            <h3>اجمالى التطبيق</h3>
            <p>اجمالى مبالغ التطبيق</p>
        </div>

    </div>
</div>
    @endif









@endsection