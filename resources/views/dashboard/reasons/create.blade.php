@extends('dashboard.master',[
                                'page_title'       => 'أسباب التواصل'
                                ])

@section('content')

    <!-- form start -->
    {!! Form::model($model,[
                            'action'=>'ReasonsmsgController@store',
                            'id'=>'myForm',
                            'role'=>'form',
                            'method'=>'POST'
                            ])!!}
        @include('dashboard.reasons.form')
        <button type="submit" class="btn btn-primary">حفظ</button>
    {!! Form::close()!!}
@stop