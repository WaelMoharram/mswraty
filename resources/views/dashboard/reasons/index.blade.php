@extends('dashboard.master',[
                                'page_title'       => 'أسباب التواصل'
                                ])
@section('content')
    @include('flash::message')
    @if(count($reasons))
        <div class="col-md-12">
            <a href="{{url('admin/reason/create')}}" class="btn btn-info">اضافة سبب جديد</a>
        </div>
        <br>
        <br>
        <div class="clearfix"></div>
        <div class="table-responsive">
            <table class="table table-bordered">
                <thead>
                <th>#</th>
                <th>السبب بالعربية </th>
                <th>السبب بالانجليزية</th>
                <th class="text-center">تعديل</th>
                <th class="text-center">حذف</th>
                </thead>
                <tbody>
                @php $count = 1; @endphp
                @foreach($reasons as $reason)
                    <tr id="removable{{$reason->id}}">
                        <td>{{$count}}</td>
                        <td>{{$reason->name_ar}}</td>
                        <td>{{$reason->name_en}}</td>
                        <td class="text-center"><a href="reason/{{$reason->id}}/edit" class="btn btn-xs btn-success"><i class="fa fa-edit"></i></a></td>
                        <td class="center">
                            <a class="btn btn-xs btn-danger" data-toggle="modal" data-target="#delete{{$reason->id}}"><i class="entypo-trash"></i></a>
                        </td>
                        {{--<td class="text-center">--}}
                        {{--<button id="{{$reason->id}}" data-token="{{ csrf_token() }}" data-route="{{URL::route('reason.destroy',$reason->id)}}"  type="button" class="destroy btn btn-danger btn-xs"><i class="fa fa-trash-o"></i></button>--}}
                        {{--</td>--}}






                    </tr>
                    <div id="delete{{$reason->id}}" class="modal fade" role="dialog">
                        <div class="modal-dialog">
                            <!-- Modal content-->
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title">حذف السبب</h4>
                                </div>
                                <div class="modal-body">
                                    <p>هل أنت متأكد من حذف السبب {{$reason->name_ar}} ؟</p>
                                </div>
                                <div class="modal-footer">
                                    {!! Form::model($reason,[
                                'action'=>['ReasonsmsgController@destroy',$reason->id],
                                'id'=>'myForm',
                                'role'=>'form',
                                'method'=>'delete'
                                ])!!}
                                    <button type="submit" class="btn btn-danger">حذف</button>
                                    {!! Form::close()!!}
                                    <button type="button" class="btn btn-default" data-dismiss="modal">لا</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    @php $count ++; @endphp
                @endforeach
                </tbody>
            </table>
        </div>
        {!! $reasons->render() !!}
    @endif
@endsection