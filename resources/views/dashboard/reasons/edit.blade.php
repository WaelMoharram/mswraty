@extends('dashboard.master',[
                                'page_title'       => 'أسباب التواصل'
                                ])

@section('content')
    <!-- form start -->
    {!! Form::model($model,[
                            'action'=>['ReasonsmsgController@update',$model->id],
                            'id'=>'myForm',
                            'role'=>'form',
                            'method'=>'PUT'
                            ])!!}
    @include('dashboard.reasons.form')
    <button type="submit" class="btn btn-primary">حفظ</button>
    {!! Form::close()!!}
@stop