@include('dashboard.partials.validation-errors')
@include('flash::message')

<div class="form-group">
    <label for="">السبب بالعربية</label>
    {!! Form::text('name_ar', null ,[
        'class' => 'form-control',
        'placeholder' => 'السبب'
    ])!!}
</div>

<div class="form-group">
    <label for="">السبب بالانجليزية</label>
    {!! Form::text('name_en', null ,[
        'class' => 'form-control',
        'placeholder' => 'Reason'
    ])!!}
</div>




