@extends('dashboard.master',[
                                'page_title'       => 'رسائل التواصل'
                                ])
@section('content')
    @include('flash::message')
    @if(count($messages))

        <div class="clearfix"></div>
        <div class="table-responsive">
            <table class="table table-bordered">
                <thead>
                <th>#</th>
                <th>البريد الالكترونى</th>
                <th>السبب التواصل</th>
                <th>الرسالة</th>
                <th>تاريخ الرسالة</th>
                <th>وقت الرسالة</th>
                <th class="text-center">حذف</th>
                </thead>
                <tbody>
                @php $count = 1; @endphp
                @foreach($messages as $message)
                    <tr id="removable{{$message->id}}">
                        <td>{{$count}}</td>
                        <td>{{$message->email}}</td>
                        <td>{{ optional($message->reasonmsg)->name_ar }}</td>
                        <td>{{$message->message}}</td>
                        <td>{{date("d-m-Y", strtotime($message->created_at))}}</td>
                        <td>{{date("h:i:s", strtotime($message->created_at))}}</td>
                        <td class="center">
                            <a class="btn btn-xs btn-danger" data-toggle="modal" data-target="#delete{{$message->id}}"><i class="entypo-trash"></i></a>
                        </td>
                        {{--<td class="text-center">--}}
                        {{--<button id="{{$message->id}}" data-token="{{ csrf_token() }}" data-route="{{URL::route('message.destroy',$message->id)}}"  type="button" class="destroy btn btn-danger btn-xs"><i class="fa fa-trash-o"></i></button>--}}
                        {{--</td>--}}






                    </tr>
                    <div id="delete{{$message->id}}" class="modal fade" role="dialog">
                        <div class="modal-dialog">
                            <!-- Modal content-->
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title">حذف الرسالة</h4>
                                </div>
                                <div class="modal-body">
                                    <p>هل أنت متأكد من حذف الرسالة ؟</p>
                                </div>
                                <div class="modal-footer">
                                    {!! Form::model($message,[
                                'action'=>['ContactusController@destroy',$message->id],
                                'id'=>'myForm',
                                'role'=>'form',
                                'method'=>'delete'
                                ])!!}
                                    <button type="submit" class="btn btn-danger">حذف</button>
                                    {!! Form::close()!!}
                                    <button type="button" class="btn btn-default" data-dismiss="modal">لا</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    @php $count ++; @endphp
                @endforeach
                </tbody>
            </table>
        </div>
        {!! $messages->render() !!}
    @endif
@endsection