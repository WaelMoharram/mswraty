@extends('dashboard.master',[
                                'page_title'       => 'المدن'
                                ])
@section('content')
    @include('flash::message')
    @if(count($cities))
        <div class="col-md-12">
            <a href="{{url('admin/city/create')}}" class="btn btn-info">اضافة مدينة جديدة</a>
        </div>
        <br>
        <br>
        <div class="clearfix"></div>
        <div class="table-responsive">
            <table class="table table-bordered">
                <thead>
                <th>#</th>
                <th>الاسم بالعربية</th>
                <th>الاسم بالانجليزية</th>
                <th class="text-center">تعديل</th>
                <th class="text-center">حذف</th>
                </thead>
                <tbody>
                @php $count = 1; @endphp
                @foreach($cities as $city)
                    <tr id="removable{{$city->id}}">
                        <td>{{$count}}</td>
                        <td>{{$city->name_ar}}</td>
                        <td>{{$city->name_en}}</td>
                        <td class="text-center"><a href="city/{{$city->id}}/edit" class="btn btn-xs btn-success"><i class="fa fa-edit"></i></a></td>
                        <td class="center">
                            <a class="btn btn-xs btn-danger" data-toggle="modal" data-target="#delete{{$city->id}}"><i class="entypo-trash"></i></a>
                        </td>
                        {{--<td class="text-center">--}}
                        {{--<button id="{{$city->id}}" data-token="{{ csrf_token() }}" data-route="{{URL::route('city.destroy',$city->id)}}"  type="button" class="destroy btn btn-danger btn-xs"><i class="fa fa-trash-o"></i></button>--}}
                        {{--</td>--}}






                    </tr>
                    <div id="delete{{$city->id}}" class="modal fade" role="dialog">
                        <div class="modal-dialog">
                            <!-- Modal content-->
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title">حذف المدينة</h4>
                                </div>
                                <div class="modal-body">
                                    <p>هل أنت متأكد من حذف المدينة {{$city->name_ar}} ؟</p>
                                </div>
                                <div class="modal-footer">
                                    {!! Form::model($city,[
                                'action'=>['CityController@destroy',$city->id],
                                'id'=>'myForm',
                                'role'=>'form',
                                'method'=>'delete'
                                ])!!}
                                    <button type="submit" class="btn btn-danger">حذف</button>
                                    {!! Form::close()!!}
                                    <button type="button" class="btn btn-default" data-dismiss="modal">لا</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    @php $count ++; @endphp
                @endforeach
                </tbody>
            </table>
        </div>
        {!! $cities->render() !!}
    @endif
@endsection