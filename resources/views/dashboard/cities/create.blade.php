@extends('dashboard.master',[
                                'page_title'       => 'المدن'
                                ])

@section('content')

    <!-- form start -->
    {!! Form::model($model,[
                            'action'=>'CityController@store',
                            'id'=>'myForm',
                            'role'=>'form',
                            'method'=>'POST'
                            ])!!}
        @include('dashboard.cities.form')
        <button type="submit" class="btn btn-primary">حفظ</button>
    {!! Form::close()!!}
@stop