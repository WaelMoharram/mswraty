@include('dashboard.partials.validation-errors')
@include('flash::message')

<div class="form-group">
    <label for="">الاسم بالعربية</label>
    {!! Form::text('name_ar', null ,[
        'class' => 'form-control',
        'placeholder' => 'الاسم'
    ])!!}
</div>

<div class="form-group">
    <label for="">الاسم بالانجليزية</label>
    {!! Form::text('name_en', null ,[
        'class' => 'form-control',
        'placeholder' => 'City name'
    ])!!}
</div>




