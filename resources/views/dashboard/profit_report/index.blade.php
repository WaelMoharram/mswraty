@extends('dashboard.master',[
                                'page_title'       => 'تقرير الارباح'
                                ])
@section('content')
    @include('flash::message')
    @if(count($photographers))

        <div class="table-responsive">
            <table class="table table-bordered">
                <thead>
                <th>#</th>
                <th>الاسم</th>
                <th>اجمالى العمليات</th>
                <th>المستحق للمصور</th>
                <th>المستحق للتطبيق</th>
                <th class="text-center">التفاصيل</th>
                </thead>
                <tbody>
                @php $count = 1; @endphp
                @foreach($photographers as $photographer)
                   @php


                   if($photographer->image == '' || $photographer->image == Null){
                        $photographer->image = "uploads/image_placeholder.png";
                   }
                   if($photographer->cover_image == '' || $photographer->cover_image == Null){
                        $photographer->cover_image = "uploads/cover_placeholder.png";
                   }
                    @endphp
                    <tr id="removable{{$photographer->id}}">
                        <td>{{$count}}</td>
                        <td><a class="btn btn-success btn-xs" data-toggle="modal" data-target="#show{{$photographer->id}}">{{$photographer->name}}</a></td>
                        <td>{{$photographer->transactions()->sum('transactions.amount')}}</td>
                        <td>{{$photographer->transactions()->sum('transactions.photographer_amount')}}</td>
                        <td>{{$photographer->transactions()->sum('transactions.app_amount')}}</td>
                        <td>
                            {!! Form::model($photographer,[
                                'action'=>['HomeController@photographer_transactions',$photographer->id],
                                'id'=>'myForm',
                                'role'=>'form',
                                'method'=>'get'
                                ])!!}
                            <button type="submit" class="btn btn-info btn-xs">التفاصيل</button>
                            {!! Form::close()!!}
                        </td>
                    </tr>
                   <div id="show{{$photographer->id}}" id="model-3" class="modal fade custom-width" role="dialog">
                       <div class="modal-dialog" style="width: 96%">
                           <!-- Modal content-->
                           <div class="modal-content">
                               <div class="modal-header">
                                   <button type="button" class="close" data-dismiss="modal">&times;</button>
                                   <h4 class="modal-title">{{$photographer->name}}</h4>
                               </div>
                               <div class="modal-body">
                                   <div class="profile-env">
                                   <header class="row">
                                       <div class="col-md-12" style="height: 400px; overflow: hidden;">
                                           <img style="margin: 0 auto;" src="../../{{$photographer->cover_image}}" class="img-responsive"/>
                                       </div>
                                       <div class="col-sm-2">

                                           <a href="#" class="profile-picture">

                                               <img src="../../{{$photographer->image}}" class="img-responsive img-circle" style="height: 150px;"/>
                                           </a>

                                       </div>

                                       <div class="col-sm-7">

                                           <ul class="profile-info-sections">
                                               <li>
                                                   <div class="profile-name">
                                                       <strong>
                                                           <a href="#">{{$photographer->name}}</a>
                                                           <!-- User statuses available classes "is-online", "is-offline", "is-idle", "is-busy" -->						</strong>
                                                       <span>مصور</span>
                                                       <span >{{$photographer->gender}}</span>
                                                   </div>
                                               </li>

                                               <li>
                                                   <div class="profile-stat">
                                                       <h3>{{$photographer->rate}}</h3>
                                                       <span><a href="#">التقييم</a></span>
                                                   </div>
                                               </li>

                                               <li>
                                                   <div class="profile-stat">
                                                       <h3>{{$photographer->rate_count}}</h3>
                                                       <span><a href="#">عدد التقييمات </a></span>
                                                   </div>
                                               </li>

                                               <li>
                                                   <div class="profile-stat">
                                                       <h3>{{$photographer->min_price}}</h3>
                                                       <span><a href="#">أقل سعر لخدمة  </a></span>
                                                   </div>
                                               </li>
                                           </ul>

                                       </div>

                                       <div class="col-sm-3">

                                       </div>

                                   </header>

                                   <section class="profile-info-tabs" style="margin-left: 0px; margin-right: 0px; ">

                                       <div class="row">

                                           <div class="col-sm-offset-2 col-sm-10">

                                               <ul class="user-details">
                                                   <li>
                                                       <a href="#">
                                                           <i class="entypo-email"></i>
                                                           {{$photographer->email}}
                                                       </a>
                                                   </li>

                                                   <li>
                                                       <a href="#">
                                                           <i class="entypo-mobile"></i>
                                                           {{$photographer->mobile}}
                                                       </a>
                                                   </li>

                                                   <li>
                                                       <a href="#">
                                                           <i class="entypo-location"></i>
                                                           {{ optional($photographer->city)->name_ar }}
                                                       </a>
                                                   </li>

                                                   <li>
                                                       <a href="#">
                                                           <i class="entypo-suitcase"></i>
                                                           {{$photographer->description}}
                                                       </a>
                                                   </li>

                                                   <li>
                                                       <a href="#">
                                                           <i class="entypo-chart-area"></i>
                                                           {{$photographer->from_year}}
                                                       </a>
                                                   </li>

                                                   <li>
                                                       <a href="#">
                                                           <i class="entypo-docs"></i>
                                                           {{$photographer->categories_human}}
                                                       </a>
                                                   </li>
                                               </ul>
                                           </div>

                                       </div>

                                   </section>

                                   </div>


                               </div>

                           </div>
                       </div>
                   </div>


                    @php $count ++; @endphp
                @endforeach
                </tbody>
            </table>
        </div>
        {!! $photographers->render() !!}
    @endif
@endsection