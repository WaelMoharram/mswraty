<!DOCTYPE html>
<html lang="en" dir="rtl">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="description" content="Neon Admin Panel" />
    <meta name="author" content="" />

    <link rel="icon" href="assets/images/favicon.ico">

    <title>تسجيل دخول | لوحة تحكم تطبيق مصوراتى</title>

    <link rel="stylesheet" href="{{asset('neon/assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css')}}'">
    <link rel="stylesheet" href="{{asset('neon/assets/css/font-icons/entypo/css/entypo.css')}}">
    <link rel="stylesheet" href="{{asset('neon/assets/css/font-icons/font-awesome/css/font-awesome.css')}}">
    <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic">
    <link rel="stylesheet" href="{{asset('neon/assets/css/bootstrap.css')}}">
    <link rel="stylesheet" href="{{asset('neon/assets/css/neon-core.css')}}">
    <link rel="stylesheet" href="{{asset('neon/assets/css/neon-theme.css')}}">
    <link rel="stylesheet" href="{{asset('neon/assets/css/neon-forms.css')}}">
    <link rel="stylesheet" href="{{asset('neon/assets/css/neon-rtl.css')}}">
    <link rel="stylesheet" href="{{asset('neon/assets/css/custom.css')}}">
    <script src="{{asset('neon/assets/js/jquery-1.11.3.min.js')}}"></script>
    <!--[if lt IE 9]><script src="{{asset('neon/assets/js/ie8-responsive-file-warning.js')}}"></script><![endif]-->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->


</head>
<body class="page-body login-page login-form-fall" data-url="http://neon.dev">


<!-- This is needed when you send requests via Ajax -->
<script type="text/javascript">
    var baseurl = '';
</script>

<div class="login-container">

    <div class="login-header login-caret">

        <div class="login-content">

            <a href="#" class="logo">
                <h2>مصوراتى</h2>
            </a>

            <p class="description">عزيزى المستخدم , برجاء تسجيل الدخول للوحة تحكم تطبيق مصوراتى</p>


        </div>

    </div>



    <div class="login-form">

        <div class="login-content">

            <div class="form-login-error">
                @if ($errors->has('email'))
                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                @endif

                    @if ($errors->has('password'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                    @endif

            </div>

            <form method="post"  action="{{ route('login') }}">
                {{ csrf_field() }}

                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">

                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="entypo-user"></i>
                        </div>

                        <input type="email" class="form-control" name="email" id="username" placeholder="البريد الالكترونى" value="{{ old('email') }}"  autocomplete="off" required autofocus/>
                    </div>

                </div>

                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">

                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="entypo-key"></i>
                        </div>

                        <input type="password" class="form-control" name="password" id="password" placeholder="كلمة المرور" autocomplete="off" required/>
                    </div>

                </div>

                <div class="form-group col-md-6 col-md-offset-3">
                    <div class="input-group">
                        <div class="input-group-addon">
                            تذكرنى
                        </div>

                        <input class="" type="checkbox" name="remember" id="password" {{ old('remember') ? 'checked' : '' }}>
                    </div>

                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-primary btn-block btn-login">
                        <i class="entypo-login"></i>
                        تسجيل الدخول
                    </button>
                </div>




            </form>



        </div>

    </div>

</div>


<!-- Bottom scripts (common) -->
<script src="{{asset('neon/assets/js/gsap/TweenMax.min.js')}}"></script>
<script src="{{asset('neon/assets/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js')}}"></script>
<script src="{{asset('neon/assets/js/bootstrap.js')}}"></script>
<script src="{{asset('neon/assets/js/joinable.js')}}"></script>
<script src="{{asset('neon/assets/js/resizeable.js')}}"></script>
<script src="{{asset('neon/assets/js/neon-api.js')}}"></script>
<script src="{{asset('neon/assets/js/jquery.validate.min.js')}}"></script>
<script src="{{asset('neon/assets/js/neon-login.js')}}"></script>


<!-- JavaScripts initializations and stuff -->
<script src="{{asset('neon/assets/js/neon-custom.js')}}"></script>


<!-- Demo Settings -->
<script src="{{asset('neon/assets/js/neon-demo.js')}}"></script>

</body>
</html>